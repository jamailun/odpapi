﻿using System;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class NodeAnimate : ITransitionNode {

		public float Duration { get; private set; }
		public float BeginTime { get; private set; }
		
		public string TransitionFill { get; private set; }
		public string AttributeName { get; private set; }

		public string[] Values { get; private set; }			//TODO interpréter les valeurs ?
		public float[] KeyTimes { get; private set; }

		public string TargetSubItem { get; private set; }
		public string TargetElementID { get; private set; }

		public NodeAnimate(ISlide root, XElement element, float beginTime) {
			BeginTime = beginTime;
			Duration = TimingRoot.StringToDuration( XmlParser.GetAttributeValue(element, "dur") );

			TransitionFill = XmlParser.GetAttributeValueNullable(element, "fill");
			AttributeName = XmlParser.GetAttributeValue(element, "attributeName");
			TargetSubItem = XmlParser.GetAttributeValueNullable(element, "sub-item");
			TargetElementID = XmlParser.GetAttributeValue(element, "targetElement");

			Values = XmlParser.GetAttributeValue(element, "values").Split(';');
			string[] timeKeysStr = XmlParser.GetAttributeValue(element, "keyTimes").Split(';');
			KeyTimes = new float[timeKeysStr.Length];

			if (Values.Length != KeyTimes.Length)
				throw new Exception("new ElementAnimation > values & keystime doesn't have the same number of elements (" + Values.Length + " and " + KeyTimes.Length +").");

			for (int i = 0; i < Values.Length; i++) {
				KeyTimes[i] = float.Parse(timeKeysStr[i]);
			}
		}

		public override string ToString() {
			return $"NodeAnimate{{attribute={AttributeName}, "
				+ (TargetElementID != null ? "targetID=" + TargetElementID + "," : "")
				+ (TargetSubItem != null ? "subID=" + TargetSubItem + ", " : "")
				+ (TransitionFill != null ? "fill=" + TransitionFill + ", " : "")
				+ "duration=" + Duration + ", begin=" + BeginTime
				+ "}";
		}

	}
}
