﻿using System;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class BeginSlideAnimation {

		public NodeTransitionFilter Transition { get; private set; }

		public BeginSlideAnimation(ISlide root, XElement element) {

			foreach(XElement child in XmlParser.GetChildren(element)) {
				string name = child.Name.LocalName;
				if(name == "transitionFilter") {
					//DebugProgram.Debug("     + NodeTransitionFilter");
					Transition = new NodeTransitionFilter(root, child, 0); //TODO use id1.begin ? 
					continue;
				}
				throw new Exception("new BeginSlideAnimation > unknown node name : '" + name + "'.");
			}

		}

	}
}
