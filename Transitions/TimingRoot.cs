﻿using System.Xml.Linq;
using ODP.API;

/**
 * Racine principale des transitions
*/
namespace ODP.Transitions {
	public class TimingRoot : ITransitionsTimingRoot {
		
		public ITransitionNode AnimationOnStart { get; private set; }
		public TransitionsSequenceXml RawSequence { get; private set; }
		public ITransitionsSequence Sequence { get; private set; }

		private readonly ISlide root;

		internal TimingRoot(ISlide root, XElement element) {
			//if (!root.HasXmlID())
			//	throw new System.Exception($"It seems slide {root.Name} doesn't have an XmlID. But it contains an attribute '{element.Name.LocalName}'.");
			// En fait on peut faire sans donc pas grave x)
			this.root = root;
			foreach (XElement child in XmlParser.GetChildren(element)) {
				if (child.Name.LocalName == "par") { // animation initiale
					AddBegin(child);
					continue;
				}
				if (child.Name.LocalName == "seq") { // séquence des éléments du slide
					AddMainSequence(child);
					continue;
				}
				throw new System.Exception($"new TImingRoot > unexpected child name : '{child.Name.LocalName}'.");
			}
		}

		private void AddBegin(XElement element) {
			string begin = XmlParser.GetAttributeValue(element, "begin");
			if (begin != root.XmlID + ".begin")
				throw new System.Exception("new TimingRoot > AddBegin : unexpected begin value : '" + begin + "'.");
			BeginSlideAnimation beginAnimation = new BeginSlideAnimation(root, element);
			AnimationOnStart = beginAnimation.Transition;
		}

		private void AddMainSequence(XElement element) {
			string nodeType = XmlParser.GetAttributeValue(element, "node-type");
			if (nodeType != "main-sequence")
				throw new System.Exception("new TimingRoot > AddMainSequence : unexpected node-type value : '" + nodeType + "'.");
			RawSequence = new TransitionsSequenceXml(root, element);

			Sequence = new TransitionsSequence(RawSequence);
		}
		
		internal static TransitionType StringToType(string value) {
			switch (value) {
				case "0s":
				case "next": return TransitionType.Next;
				case "on-click": return TransitionType.OnClick;
				case "with-previous": return TransitionType.SameTime;
				case "after-previous": return TransitionType.AfterFinished;
				default: throw new System.Exception("Unknown TransitionType : '" + value + "'.");
			}
		}

		internal static float StringToDuration(string value) {
			return float.Parse(value.Remove(value.Length - 1).Replace('.',','));
		}

		public override string ToString() {
			return "TimingRoot["
				+ (AnimationOnStart == null ? "" : "\n\ton-start=" +AnimationOnStart.ToString()+",\n\t")
				+ (Sequence == null ? "" : "\n\tsequence=" + Sequence.ToString())
				+ "\n]";
		}
	}
	
}
