﻿using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class NodeTransitionFilter : ITransitionNode {

		// transitionFilter smil:dur="2s"/>
		public string TransitionDirection { get; private set; }
		public string TransitionType { get; private set; }
		public string TransitionSubType { get; private set; }
		// optional
		public string TargetSubItem { get; private set; }
		// ITransitionNode
		public string TargetElementID { get; private set; }
		public float Duration { get; private set; }
		public float BeginTime { get; private set; }

		internal NodeTransitionFilter(ISlide root, XElement element, float beginTime) {
			TransitionDirection = XmlParser.GetAttributeValueNullable(element, "direction");
			TransitionType = XmlParser.GetAttributeValue(element, "type");
			TransitionSubType = XmlParser.GetAttributeValue(element, "subtype");

			TargetSubItem = XmlParser.GetAttributeValueNullable(element, "sub-item");
			TargetElementID = XmlParser.GetAttributeValueNullable(element, "targetElement") ?? root.XmlID;

			string duraString = XmlParser.GetAttributeValue(element, "dur");
			Duration = float.Parse(duraString.Replace('.',',').Remove(duraString.Length - 1));
			string beginString = XmlParser.GetAttributeValueNullable(element, "begin");
			if(beginString != null) {
				BeginTime = float.Parse(beginString.Remove(beginString.Length - 1));
			} else {
				BeginTime = beginTime;
			}
		}

		public override string ToString() {
			return $"NodeTransitionFilter{{targetID={TargetElementID}, "
				+ (TargetElementID != null ? "targetID=" + TargetElementID + ", " : "")
				+ (TargetSubItem != null ? "subItem=" + TargetSubItem + ", " : "")
				+ (TransitionDirection != null ? "direction=" + TransitionDirection + "," : "")
				+ (TransitionType != null ? "type=" + TransitionType + ", " : "")
				+ (TransitionSubType != null ? "subType=" + TransitionSubType + ", " : "")
				+ "duration=" + Duration + ", begin=" + BeginTime
				+ "}";
		}
	}
}
