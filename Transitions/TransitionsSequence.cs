﻿using System.Collections.Generic;
using System.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class TransitionsSequence : ITransitionsSequence {

		private int index = 0;
		private IList<IList<ITransitionNode>> blocks = new List<IList<ITransitionNode>>();
		private IList<TransitionCondition> conditions = new List<TransitionCondition>();

		internal TransitionsSequence(TransitionsSequenceXml xmlSequence) {
			// Build all the blocks from the xml like data-structure.
			foreach(SequencesBlock sb in xmlSequence.GetSequencesBlocks()) {
				// (useless here >) TransitionType blockType = sb.Type;
				foreach(NodesSequence ns in sb.GetNodesSequences()) {
					TransitionType sequenceType = ns.Type;
					float dataTime = ns.BeginTime;
					// Par soucis de simplification pour le moment, on considère qu'ils vont tous se passer en même temps
					IList<ITransitionNode> nodes = new List<ITransitionNode>( ns.GetNodes() );

					// Ajout d'un bloc dans "l'itérateur".
					blocks.Add(nodes);
					conditions.Add(new TransitionCondition {
						Type = (sequenceType == TransitionType.OnClick) ? TransitionConditionType.WaitClick : TransitionConditionType.WaitMillis,
						MillisParam = (sequenceType == TransitionType.SameTime) ? 0 : dataTime * 1000f
					});
				}
			}
		}

		public bool ConditionAccepted() {
			if ( ! HasMore() )
				return false;
			index++;
			return true;
		}

		public bool ForcePrevious() {
			if (index == 0)
				return false;
			index--;
			return true;
		}

		public TransitionCondition GetCurrentCondition() {
			return conditions[index];
		}

		public IEnumerable<ITransitionNode> GetCurrentTransitionsBlock() {
			return blocks[index].AsEnumerable();
		}

		public bool HasMore() {
			return index < blocks.Count - 1;
		}

		public override string ToString() {
			var str = "TransitionSequence[";
			for(int i = 0; i < blocks.Count; i++) {
				IList<ITransitionNode> ltn = blocks[i];
				TransitionCondition cdt = conditions[i];
				str += $"\n\t\t{i} > {{{cdt.Type}, millis={cdt.MillisParam}}} => [\n";
				foreach(var tr in ltn) {
					str += "\t\t\t" + tr.ToString() + ",\n";
				}
				str += "\t\t],";
			}
			return str + "\n\t]";
		}
	}
}
