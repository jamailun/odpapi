﻿using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class NodeSet : ITransitionNode {

		public float Duration { get; private set; }
		public float BeginTime { get; private set; }

		public string TransitionFill { get; private set; }
		public string AttributeName { get; private set; }
		public string AttributeValue { get; private set; }

		public string TargetSubItem { get; private set; }
		public string TargetElementID { get; private set; }

		internal NodeSet(ISlide root, XElement element) {
			Duration = TimingRoot.StringToDuration(XmlParser.GetAttributeValue(element, "dur"));

			BeginTime = TimingRoot.StringToDuration(XmlParser.GetAttributeValue(element, "begin"));

			TransitionFill = XmlParser.GetAttributeValue(element, "fill");
			AttributeName = XmlParser.GetAttributeValue(element, "attributeName");
			AttributeValue = XmlParser.GetAttributeValue(element, "to");

			TargetSubItem = XmlParser.GetAttributeValueNullable(element, "sub-item"); // sub item : si cible = slide je pense 
			TargetElementID = XmlParser.GetAttributeValue(element, "targetElement");
		}

		public override string ToString() {
			return $"NodeSet{{attribute={AttributeName}, to={AttributeValue}, "
				+ (TargetElementID != null ? "targetID=" + TargetElementID + "," : "")
				+ (TargetSubItem != null ? "subID=" + TargetSubItem + ", " : "")
				+ (TransitionFill != null ? "fill=" + TransitionFill + ", " : "")
				+ "duration=" + Duration + ", begin=" + BeginTime
				+ "}";
		}
	}
}
