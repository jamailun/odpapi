﻿namespace ODP.Transitions {
	public enum TransitionType {
		Next,
		SameTime,
		OnClick,
		AfterFinished
	}
}
