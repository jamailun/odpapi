﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class TransitionsSequenceXml {

		private readonly IList<SequencesBlock> sequences;

		public TransitionsSequenceXml(ISlide root, XElement element) {
			sequences = new List<SequencesBlock>();

			foreach (XElement child in XmlParser.GetChildren(element)) {
				string name = child.Name.LocalName;
				if (name != "par")
					throw new System.Exception("new TransitionSequence > unknown child name : '" + name + "'.");
				sequences.Add(new SequencesBlock(root, child));
			}
		}

		public IEnumerable<SequencesBlock> GetSequencesBlocks() {
			return sequences.AsEnumerable();
		}
	}
}
