﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class NodesSequence {

		private readonly IList<ITransitionNode> effects;

		public float BeginTime { get; private set; }
		public TransitionType Type { get; }

		internal NodesSequence(ISlide root, XElement element, float additionalBegin) {
			effects = new List<ITransitionNode>();
			
			BeginTime = additionalBegin + TimingRoot.StringToDuration(XmlParser.GetAttributeValue(element, "begin"));

			Type = TimingRoot.StringToType(XmlParser.GetAttributeValue(element, "node-type"));

			foreach(XElement child in XmlParser.GetChildren(element)) {
				string name = child.Name.LocalName;
				switch (name) {
					case "set":
						ITransitionNode nodeSet = new NodeSet(root, child);
						effects.Add(nodeSet);
						//DebugProgram.Debug("     + " + nodeSet);
						break;
					case "transitionFilter":
						ITransitionNode nodeTf = new NodeTransitionFilter(root, child, BeginTime);
						effects.Add(nodeTf);
						//DebugProgram.Debug("     + " + nodeTf);
						break;
					case "animate":
						ITransitionNode nodeAnim = new NodeAnimate(root, child, BeginTime);
						effects.Add(nodeAnim);
						//DebugProgram.Debug("     + " + nodeAnim);
						break;
					case "animateTransform":
						// TODO ajouteer les animation sur la transform (la rotation par exemple)
						// extrait :
						//<anim:animateTransform smil:fill="hold" svg:type="rotate" smil:dur="0.7s" smil:attributeName="transform" smil:targetElement="id37" smil:by="-360"/>
						break;
					default:
						throw new Exception("new SequenceRoot > unexpected node name ('"+name+"'.");
				}
			}

		}

		public IEnumerable<ITransitionNode> GetNodes() {
			return effects.AsEnumerable();
		}

	}
}
