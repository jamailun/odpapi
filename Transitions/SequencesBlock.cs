﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Transitions {
	public class SequencesBlock {

		private readonly IList<NodesSequence> children;

		public TransitionType Type { get; private set; }

		internal SequencesBlock(ISlide root, XElement element) {
			children = new List<NodesSequence>();

			Type = TimingRoot.StringToType(XmlParser.GetAttributeValue(element, "begin"));
			foreach (XElement child in XmlParser.GetChildren(element)) {
				string name = child.Name.LocalName;
				if (name != "par")
					throw new Exception("new TransitionSequence > unknown child name : '" + name + "'.");

				float blockBegin = TimingRoot.StringToDuration(XmlParser.GetAttributeValue(child, "begin"));
				foreach(XElement subChild in XmlParser.GetChildren(child)) {
					if (subChild.Name.LocalName != "par")
						throw new Exception("new TransitionSequence > foreach > unknown child name : '" + subChild.Name.LocalName + "'.");
					children.Add(new NodesSequence(root, subChild, blockBegin));
				}
			}
		}

		public IEnumerable<NodesSequence> GetNodesSequences() {
			return children.AsEnumerable();
		}

	}
}
