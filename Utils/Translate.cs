﻿namespace ODP.Utils {
	public class Translate {

		//cf https://developer.mozilla.org/fr/docs/Web/CSS/transform-function/skewX
		public Size HorizontalDistortion { get; }

		// only handle from center of image
		public Size Rotation { get; }

		// Translations
		public Size TranslationX { get; }
		public Size TranslationY { get; }

		public Translate(string value) {
			string[] words = value.Replace("(", "").Replace(")", "").Split(' ');
			int i = 0;
			while(i < words.Length) {
				string current = words[i];
				i++;
				switch (current) {
					case "rotate":
						Rotation = Size.ParseString(words[i] + "rad");
						i++;
						break;
					case "translate":
						TranslationX = Size.ParseString(words[i]);
						TranslationY = Size.ParseString(words[i+1]);
						i += 2;
						break;
					default:
						throw new System.Exception("Unknwown command... : " + current);
				}
			}
			/*HorizontalDistortion = Size.ParseString(words[1] + "rad");
			
			if(words.Length < 6) {
				TranslationX = new Size(0, StyleUnit.Centimeter);
				TranslationY = new Size(0, StyleUnit.Centimeter);
				return;
			}
			TranslationX = Size.ParseString(words[5]);
			TranslationY = Size.ParseString(words[6]);*/
		}

	}
}
