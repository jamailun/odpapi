﻿using System.Collections.Generic;

namespace ODP.Utils {
	public class SquareSize {
		private Dictionary<SquareSizeType, Size> margins;
		private SquareSizeType[] types = new SquareSizeType[] { SquareSizeType.Bottom, SquareSizeType.Top, SquareSizeType.Left, SquareSizeType.Right };

		public SquareSize() {
			margins = new Dictionary<SquareSizeType, Size>();
		}

		public Size GetSize(SquareSizeType type) {
			return margins[type];
		}

		public bool HasSize(SquareSizeType type) {
			return margins.ContainsKey(type);
		}

		public bool HasAnySize() {
			if (margins.Count == 0)
				return false;
			foreach(var si in margins) {
				if (si.Value.Value > 0)
					return true;
			}
			return false;
		}

		public void SetSize(SquareSizeType type, Size size) {
			margins.Add(type, size);
		}

		public SquareSize GetChildrenOf(SquareSize parent) {
			SquareSize child = new SquareSize();
			foreach (SquareSizeType type in types) {
				if (parent.HasSize(type)) {
					child.SetSize(type, parent.GetSize(type));
					continue;
				}
				if (HasSize(type))
					child.SetSize(type, GetSize(type));
			}
			return child;
		}

		public enum SquareSizeType {
			Top,
			Right,
			Bottom,
			Left
		}

		public override string ToString() {
			return "SqrSize["
				+ (HasSize(SquareSizeType.Top) ? "top=" + GetSize(SquareSizeType.Top) + ";" : "")
				+ (HasSize(SquareSizeType.Right) ? "right=" + GetSize(SquareSizeType.Right) + ";" : "")
				+ (HasSize(SquareSizeType.Bottom) ? "bottom=" + GetSize(SquareSizeType.Bottom) + ";" : "")
				+ (HasSize(SquareSizeType.Left) ? "left=" + GetSize(SquareSizeType.Left) : "")
			+ "]";
		}
	}
}
