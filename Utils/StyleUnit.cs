﻿namespace ODP.Utils {
	public enum StyleUnit {
		Pixel,
		Centimeter,
		Percentage,
		Radians,
	}
}
