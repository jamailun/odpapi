﻿using System;

namespace ODP.Utils {
	public class FontStyle {

		private readonly bool[] styles;

		public FontStyle() {
			styles = new bool[ Enum.GetNames(typeof(FontStyleType)).Length ];
			for (int i = 0; i < styles.Length; i++)
				styles[i] = false;
		}

		internal void AddPropertie(FontStyleType type) {
			styles[(int)type] = true;
		}

		internal void RemovePropertie(FontStyleType type) {
			styles[(int)type] = false;
		}

		public bool HasPropertie(FontStyleType type) {
			return styles[(int)type];
		}

		public bool IsEmpty() {
			foreach (bool b in styles)
				if (b) return false;
			return true;
		}

		public enum FontStyleType : int {
			Bold = 0,
			Italic,
			Underlined,
			Linestrike,
		}

		public override string ToString() {
			return "FontStyle["
				+ (HasPropertie(FontStyleType.Bold) ? "Bold, " : "")
				+ (HasPropertie(FontStyleType.Italic) ? "Italic, " : "")
				+ (HasPropertie(FontStyleType.Underlined) ? "Underlined, " : "")
				+ (HasPropertie(FontStyleType.Linestrike) ? "Linestrike" : "")
				+ "]";
		}

	}
}
