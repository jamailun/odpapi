﻿using System;
using System.Xml.Linq;

namespace ODP.Utils {
	public class Fill {
		
		private static FillType StringToType(string type) {
			switch (type) {
				case null:
				case "none":
					return FillType.None;
				case "solid":
					return FillType.Solid;
				case "bitmap":
					return FillType.Bitmap;
				default:
					throw new Exception("ERROR. Unknown fill type : (" + type + ").");
			}
		}

		public static Fill FromType(string type) { // méthode statique pour pas surcharger le constructeur.
			return new Fill(StringToType(type));
		}

		public Fill(XElement element) {
			Type = StringToType(XmlParser.GetAttributeValueNullable(element, "fill"));
			FillColor = XmlParser.GetAttributeValueNullable(element, "fill-color") ?? "none";
		}

		public Fill(string solidColor) {
			Type = FillType.Solid;
			FillColor = solidColor ?? "none";
		}

		public Fill(FillType type) {
			Type = type;
			FillColor = "none";
		}

		public Fill(FillType type, string color) {
			Type = type;
			FillColor = color ?? "none";
		}

		public Fill MutateWithColor(string color) {
			return new Fill(Type, color);
		}
		public Fill MutateWithType(string type) {
			return new Fill(StringToType(type), FillColor);
		}

		public FillType Type { get; }

		public string FillColor { get; }

		public enum FillType {
			// No fill to be done
			None,
			// Colored fill : fill-color
			Solid,
			// Je sais pas du tout à quoi ça sert
			Bitmap
		}

		public override string ToString() {
			return "Fill[type=" +(Type == FillType.None ? "none" : "solid, color="+FillColor) + "]";
		}

	}
}
