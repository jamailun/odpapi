﻿using System;

namespace ODP.Utils {
	public class Size {

		public static Size ParseString(string valueToParse) {
			if (valueToParse == null)
				return null;
			if (valueToParse.EndsWith("cm")) {
				return new Size (
					ParseRaw(valueToParse.Substring(0, valueToParse.Length - 2)),
					StyleUnit.Centimeter
				);
			}
			if (valueToParse.EndsWith("px")) {
				return new Size(
					ParseRaw(valueToParse.Substring(0, valueToParse.Length - 2)),
					StyleUnit.Pixel
				);
			}
			if (valueToParse.EndsWith("%")) {
				return new Size(
					ParseRaw(valueToParse.Substring(0, valueToParse.Length - 1)),
					StyleUnit.Percentage
				);
			}
			if (valueToParse.EndsWith("rad")) {
				return new Size(
					ParseRaw(valueToParse.Substring(0, valueToParse.Length - 3)),
					StyleUnit.Radians
				);
			}
			DebugProgram.PrintString("ERROR. Unknown mesure : [" + valueToParse + "]");
			return null;
		}

		public static int FontSize(string value) {
			if (value.EndsWith("pt")) {
				int size = -1;
				string newValue = value.Substring(0, value.Length - 2);
				int.TryParse(newValue, out size);
				return size;
			}
			return -1;
		}

		private static float ParseRaw(string raw) {
			try {
				return float.Parse(raw.Replace('.', ',').Replace("cm",""));
			} catch (FormatException e) {
				DebugProgram.PrintString("Size > Illegal float format during ParseRaw (" + raw + ") : " + e);
				return 0;
			}
		}

		public Size(float pureValue, StyleUnit unit) {
			this.Value = pureValue;
			this.Unit = unit;
		}

		public float Value { get; }

		public StyleUnit Unit { get; }

		public string StringUnit() {
			switch(Unit) {
				case StyleUnit.Centimeter:
					return "cm";
				case StyleUnit.Percentage:
					return "%";
				case StyleUnit.Pixel:
					return "px";
				case StyleUnit.Radians:
					return "rad";
			}
			return "<ERROR>";
		}

		public override string ToString() {
			return Value+StringUnit();
		}
	}
}
