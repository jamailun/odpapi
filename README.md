# OPDAPI

## Résumé

Créé pour être utilisé dans le projet [Slidermax](https://gitlab.com/-/ide/project/jamailun/slidermax/), ODPAPI permet la lecture de fichiers `.odp`. Ce projet propose une structure contenant les diapositives du fichier de présentation, ainsi que les styles et les images associées.

## Auteur

Création originale de Timothé Rosaz, en 2020/2021.

Pour toute question : jamailun@laposte.net ou timothe.rosaz@insa-rennes.fr

## Utilisation

Consultez le [wiki](https://gitlab.com/jamailun/odpapi/-/wikis/home).
