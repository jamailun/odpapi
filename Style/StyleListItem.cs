﻿using System;
using System.Xml.Linq;
using ODP.API;
using ODP.Utils;

namespace ODP.Generics {
	public class StyleListItem : IStyleListItem {

		public const string LIST_LEVEL_PROPERTIES = "list-level-properties";
		public const string TEXT_PROPERTIES = "text-properties";

		//style: list-level-style-bullet
		private readonly string bulletChar = "●";
		private readonly int textLevel = -1;
		//style: list-level-properties
		private Size minLabelWidth = null;
		private Size spaceBefore = null;
		//style: text-properties
		private Size fontSize = null;
		private string fontFamily = "Arial";
		private bool useWindowFontColor = true;

		public StyleListItem(XElement source) {
			textLevel = int.Parse(XmlParser.GetAttributeValue(source, "level"));
			bulletChar = XmlParser.GetAttributeValue(source, "bullet-char");
			foreach (XElement element in XmlParser.GetChildren(source)) {
				string name = element.Name.LocalName;
				switch (name) {
					case LIST_LEVEL_PROPERTIES:
						AddListLevelPropertiesElement(element);
						break;
					case TEXT_PROPERTIES:
						AddTextPropertiesElement(element);
						break;
					default:
						throw new Exception("new StyleList > Unexpected tag name : " + name + ".");
				}
			}
		}

		public void AddListLevelPropertiesElement(XElement element) {
			minLabelWidth = Size.ParseString(XmlParser.GetAttributeValueNullable(element, "min-label-width"));
			spaceBefore = Size.ParseString(XmlParser.GetAttributeValueNullable(element, "space-before"));
		}

		public void AddTextPropertiesElement(XElement element) {
			fontSize = Size.ParseString(XmlParser.GetAttributeValueNullable(element, "font-size"));
			try {
				fontFamily = XmlParser.GetAttributeValue(element, "font-family");
			} catch (Exception) {
				fontFamily = XmlParser.GetAttributeValueNullable(element, "font-name");
			}

			string uwfc = XmlParser.GetAttributeValueNullable(element, "use-window-font-color");
			if(uwfc != null)
				useWindowFontColor = bool.Parse(uwfc);
	}

		public int GetTextLevel() {
			return textLevel;
		}

		public string GetBulletChar() {
			return bulletChar;
		}

		public Size GetMinLabelWidth() {
			return minLabelWidth;
		}

		public Size GetSpaceBefore() {
			return spaceBefore;
		}

		public Size GetFontSize() {
			return fontSize;
		}

		public string GetFontFamily() {
			return fontFamily;
		}

		public bool UseWindowFontColor() {
			return useWindowFontColor;
		}

		public override string ToString() {
			return "StyleListItem["
				+ "text-level='" + GetTextLevel() + "', "
				+ "bullet-char='"+GetBulletChar() + "', "
					+ "minLabelWidth='" + GetMinLabelWidth() + "', "
					+ "spaceBefore='" + GetSpaceBefore() + "', "
					+ "fontSize='" + GetFontSize() + "', "
				+ "font-family='" + GetFontFamily() + "', "
				+ "useWindowFontColor='" + UseWindowFontColor()
			+ "]";
		}
	}
}
