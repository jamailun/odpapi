﻿using System.IO;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class Texture : ITexture {

		public string Name { get; }
		public string DisplayName { get; }
		public string ImagePath { get; }
		public bool VisibleByDefault { get; set; }

		public Texture(XElement source) {
			Name = XmlParser.GetAttributeValue(source, "name");
			DisplayName = XmlParser.GetAttributeValue(source, "display-name");
			ImagePath = XmlParser.GetAttributeValue(source, "href");
			VisibleByDefault = true;
		}

		public MemoryStream GetImage() {
			return XmlParser.ReadImageFromODP(ImagePath);
		}

		public string XmlID { get; }
		public bool HasXmlID() { return false; }
		public APIXmlUnique GetXmlIdInChildren(string id) { return null; }

		public override string ToString() {
			return "Texture["
				+ "Name='" + Name + ", "
				+ "DisplayName='" + DisplayName + ", "
				+ "ImagePath='" + ImagePath + "]";
		}
	}
}
