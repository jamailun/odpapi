﻿using ODP.Utils;
using ODP.API;

namespace ODP.Internal {
	/**
	 *	Tous les styles ont les mêmes getter, qui prennent de la place pour rien. ici, je les ai tous réunis. Il n'y a aucune logique,
	 *	que des getters.
	 * 
	 *	Auteur : Timothé ROSAZ, 2021. 
	 */
	public abstract class AbstractStyle : IStyle {

		public override string ToString() {
			return "Style[name=" + Name + ", type=" + Type
				+ (GetStyleList() != null ? GetStyleList().ToString() : "") 
				+ (HasParent() ? ", parent-name=" + GetParentStyle().Name : "")
				+ (HasFontSize() ? ", font-size=" + GetFontSize()+"pt" : "")
				+ (HasFontStyle() ? ", font-style=" + GetFontStyle() : "")
				+ (HasFontName() ? ", font-name='" + GetFontName() : "'")
				+ (HasTextAlign() ? ", text-align=" + GetTextAlign() : "")
				+ (HasTextIndent() ? ", text-indent=" + GetTextIndent() : "")
				+ (HasMarginData() ? ", margin=" + GetMargin() : "")
				+ (HasPaddingData() ? ", padding=" + GetPadding() : "")
				+ (HasAreaVerticalAlign() ? ", vertical-align=" + GetAreaVerticalAlign() : "")
				+ (HasMinHeight() ? ", min-height=" + GetMinHeight() : "")
				+ (HasLineHeight() ? ", line-height=" + GetLineHeight() : "")
				+ (HasFillData() ? ", fill=" + GetFill() : "")
				+ (HasStrokeData() ? ", stroke=" + GetStroke() : "")
				+ (HasStrokeWidth() ? ", stroke-width=" + GetStrokeWidth() : "")
				+ (HasFillImage() ? ", imageFill=" + GetFillImage().DisplayName : "")
				+ "]";
		}

		public abstract IStyleList GetStyleList();
		public abstract IStyle DerivateWithChild(IStyle child);
		public abstract IStyle DerivateWithParent(IStyle parent);

		protected int fontSize = -1;
		public int GetFontSize() {
			if (HasParent())
				if (GetParentStyle().HasFontSize())
					return GetParentStyle().GetFontSize();
			return fontSize;
		}
		public bool HasFontSize() {
			return GetFontSize() > -1;
		}

		protected FontStyle fontStyles = new FontStyle();
		public FontStyle GetFontStyle() {
			if (HasParent())
				if (GetParentStyle().HasFontStyle())
					return GetParentStyle().GetFontStyle();
			return fontStyles;
		}
		public bool HasFontStyle() {
			return !GetFontStyle().IsEmpty();
		}

		protected string fontColor = "#000000";
		public string GetFontColor() {
			if (HasParent())
				if (GetParentStyle().HasFontColor())
					return GetParentStyle().GetFontColor();
			return fontColor;
		}
		public bool HasFontColor() {
			return GetFontColor() != "#000000";
		}

		protected string fontName = null;
		public string GetFontName() {
			if (HasParent())
				if (GetParentStyle().HasFontName())
					return GetParentStyle().GetFontName();
			return fontName;
		}
		public bool HasFontName() {
			return GetFontName() != null;
		}

		protected string textAlign = null;
		public string GetTextAlign() {
			if (HasParent())
				if (GetParentStyle().HasTextAlign())
					return GetParentStyle().GetTextAlign();
			return textAlign;
		}
		public bool HasTextAlign() {
			return GetTextAlign() != null;
		}

		protected Size textIndent;
		public Size GetTextIndent() {
			if (HasParent())
				if (GetParentStyle().HasTextIndent())
					return GetParentStyle().GetTextIndent();
			return textIndent;
		}
		public bool HasTextIndent() {
			return GetTextIndent() != null;
		}

		protected SquareSize margin = new SquareSize();
		public SquareSize GetMargin() {
			if (HasParent())
				if (GetParentStyle().HasMarginData())
					return margin.GetChildrenOf(GetParentStyle().GetMargin());
			return margin;
		}
		public bool HasMarginData() {
			return GetMargin().HasAnySize();
		}

		protected SquareSize padding = new SquareSize();
		public SquareSize GetPadding() {
			if (HasParent())
				if (GetParentStyle().HasPaddingData())
					return padding.GetChildrenOf(GetParentStyle().GetPadding());
			return padding;
		}
		public bool HasPaddingData() {
			return GetPadding().HasAnySize();
		}

		protected string areaVerticalAlign = null;
		public string GetAreaVerticalAlign() {
			if (HasParent())
				if (GetParentStyle().HasAreaVerticalAlign())
					return GetParentStyle().GetAreaVerticalAlign();
			return areaVerticalAlign;
		}
		public bool HasAreaVerticalAlign() {
			return GetAreaVerticalAlign() != null;
		}

		protected Size minHeight = null;
		public Size GetMinHeight() {
			if (HasParent())
				if (GetParentStyle().HasMinHeight())
					return GetParentStyle().GetMinHeight();
			return minHeight;
		}
		public bool HasMinHeight() {
			return GetMinHeight() != null;
		}

		protected Size lineHeight = null;
		public Size GetLineHeight() {
			if (HasParent())
				if (GetParentStyle().HasLineHeight())
					return GetParentStyle().GetLineHeight();
			return lineHeight;
		}
		public bool HasLineHeight() {
			return GetLineHeight() != null;
		}

		public abstract StyleType Type { get; }

		public abstract string Name { get; }

		protected Fill fill;
		public Fill GetFill() {
			if (HasParent())
				if (GetParentStyle().HasFillData())
					return GetParentStyle().GetFill();
			return fill;
		}
		public bool HasFillData() {
			return fill != null && fill.Type != Fill.FillType.None;
		}

		protected Fill stroke;
		public Fill GetStroke() {
			if (HasParent())
				if (GetParentStyle().HasStrokeData())
					return GetParentStyle().GetStroke();
			return stroke;
		}
		public bool HasStrokeData() {
			return stroke != null && stroke.Type != Fill.FillType.None;
		}

		protected Size strokeWidth = null;
		public Size GetStrokeWidth() {
			if (HasParent())
				if (GetParentStyle().HasStrokeWidth())
					return GetParentStyle().GetStrokeWidth();
			return strokeWidth;
		}
		public bool HasStrokeWidth() {
			return GetStrokeWidth() != null;
		}

		public abstract IStyle GetParentStyle();

		public abstract bool HasParent();

		protected ITexture fillImage;
		public ITexture GetFillImage() {
			if (HasParent())
				if (GetParentStyle().HasFillImage())
					return GetParentStyle().GetFillImage();
			return fillImage;
		}
		public bool HasFillImage() {
			return GetFillImage() != null;
		}

	}
}
