﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class StyleList : IStyleList {

		public const string LIST_ITEM_ELEMENT = "list-level-style-bullet";
		
		private Dictionary<int, StyleListItem> items = new Dictionary<int, StyleListItem>();

		public StyleList(XElement source) {
			Name = XmlParser.GetAttributeValue(source, "name");

			foreach(XElement eItem in XmlParser.GetChildren(source)) {
				if (eItem.Name.LocalName == "list-level-style-number") // Aucune idée de l'utilisé de cette balise...
					continue;
				if (eItem.Name.LocalName != LIST_ITEM_ELEMENT)
					throw new Exception("new StyleList > Unexpected tag name : " + eItem.Name.LocalName + ".");
				AddListItem(eItem);
			}
		}

		public IStyleListItem GetItem(int position) {
			if ( ! items.ContainsKey(position))
				return null;
			return items[position];
		}

		public IList<IStyleListItem> GetAllItems() {
			return new List<IStyleListItem>(items.Values);
		}

		private void AddListItem(XElement itemSource) {
			StyleListItem item = new StyleListItem(itemSource);
			items.Add(item.GetTextLevel(), item);
		}

		public string Name { get;  }

		public override string ToString() {
			string str = "StyleList[";
			bool first = true;
			foreach (KeyValuePair<int, StyleListItem> entry in items) {
				if (first)
					first = false;
				else
					str += ",";
				str += "\n\t" + entry.Value;
			}
			return str + "\n]";
		}

	}
}
