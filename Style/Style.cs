﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ODP.API;
using ODP.Utils;

namespace ODP.Generics {
	public class Style : Internal.AbstractStyle {

		private static readonly Dictionary<string, StyleType> TYPES = new Dictionary<string, StyleType>() {
			{ "text", StyleType.Text }, { "drawing-page", StyleType.DrawingPage }, { "paragraph", StyleType.Paragraph },
			{ "graphic", StyleType.Graphic }, { "presentation", StyleType.Presentation }
		};

		public static readonly IList<string> STYLE_PROPERTIES_NAMES = new List<string>() {
			"drawing-page-properties" , "graphic-properties", "paragraph-properties", "text-properties"
		};

		private IStyle parent;
		private IStyleList listStyle;

		public Style(IStylesLibrairie context, XElement source) {
			Name = XmlParser.GetAttributeValue(source, "name");
			Type = TYPES[XmlParser.GetAttributeValue(source, "family")];
			parent = context.GetWithName( XmlParser.GetAttributeValueNullable(source, "parent-style-name") );

			foreach(XElement element in XmlParser.GetChildren(source)) {
				string name = element.Name.LocalName;
				if(STYLE_PROPERTIES_NAMES.Contains(name)) {
					AddElement(element, context);
					continue;
				}
				throw new Exception("new Style > unexpected tag name : " + name + ".");
			}
		}


		internal static IStyle Empty() {
			return new Style();
		}

		private Style() {
			Name = "None";
			Type = StyleType.Graphic;
		}

		internal Style(IStyleList listStyle) {
			Name = listStyle.Name;
			Type = StyleType.TextList;
			this.listStyle = listStyle;
		}

		private Style(IStyle parent, IStyle child) {
			Name = child.Name;
			Type = parent.Type;
			this.parent = parent.HasParent() ? parent.GetParentStyle() : null;
			listStyle = child.GetStyleList();
			margin = child.HasMarginData() ? child.GetMargin() : parent.GetMargin() ;
			padding = child.HasPaddingData() ? child.GetPadding() : parent.GetPadding();
			textIndent = child.HasTextIndent() ? child.GetTextIndent() : parent.GetTextIndent();
			textAlign = child.HasTextAlign() ? child.GetTextAlign() : parent.GetTextAlign();
			lineHeight = child.HasLineHeight() ? child.GetLineHeight() : parent.GetLineHeight();
			fill = child.HasFillData() ? child.GetFill() : parent.GetFill();
			stroke = child.HasStrokeData() ? child.GetStroke() : parent.GetStroke();
			strokeWidth = child.HasStrokeWidth() ? child.GetStrokeWidth() : parent.GetStrokeWidth();
			minHeight = child.HasMinHeight() ? child.GetMinHeight() : parent.GetMinHeight();
			areaVerticalAlign = child.HasAreaVerticalAlign() ? child.GetAreaVerticalAlign() : parent.GetAreaVerticalAlign();
			fontColor = child.HasFontColor() ? child.GetFontColor() : parent.GetFontColor();
			fontSize = child.HasFontSize() ? child.GetFontSize() : parent.GetFontSize();
			fontName = child.HasFontName() ? child.GetFontName() : parent.GetFontName();
			fontStyles = child.HasFontStyle() ? child.GetFontStyle() : parent.GetFontStyle();
			fillImage = child.HasFillImage() ? child.GetFillImage() : parent.GetFillImage();
		//	DebugProgram.PrintString(child.Name + " -->" + parent.Name + " )=> " + ToString());
		}

		public override IStyle DerivateWithChild(IStyle child) {
			return new Style(this, child);
		}

		public override IStyle DerivateWithParent(IStyle parent) {
			return new Style(parent, this);
		}

		private void AddElement(XElement element, IStylesLibrairie context) {
			bool fillDone = false;
			foreach (XAttribute attribute in element.Attributes()) {
				string name = attribute.Name.LocalName;
				switch (name) {
					// paragraph-properties -> IGNORE(text-autospace,punctuation-wrap,line-break,font-independent-line-spacing)
					// margin
					case "margin-top":
						margin.SetSize(SquareSize.SquareSizeType.Top, Size.ParseString(attribute.Value));
						break;
					case "margin-right":
						margin.SetSize(SquareSize.SquareSizeType.Right, Size.ParseString(attribute.Value));
						break;
					case "margin-bottom":
						margin.SetSize(SquareSize.SquareSizeType.Bottom, Size.ParseString(attribute.Value));
						break;
					case "margin-left":
						margin.SetSize(SquareSize.SquareSizeType.Left, Size.ParseString(attribute.Value));
						break;
					// other
					case "text-indent":
						textIndent = Size.ParseString(attribute.Value);
						break;
					case "text-align":
						textAlign = attribute.Value;
						break;
					case "line-height":
						lineHeight = Size.ParseString(attribute.Value);
						break;
					// text-properties
					case "font-size":
						fontSize = Size.FontSize(attribute.Value);
						break;
					case "font-name":
						fontName = attribute.Value;
						break;
					case "font-weight":
						if (attribute.Value == "normal") {
							fontStyles.RemovePropertie(FontStyle.FontStyleType.Bold);
							break;
						}
						if (attribute.Value == "bold")
							fontStyles.AddPropertie(FontStyle.FontStyleType.Bold);
						else
							DebugProgram.PrintString($"ERROR : unknown bold weight : '{attribute.Value}'.");
						break;
					case "font-style":
						if (attribute.Value == "normal") {
							fontStyles.RemovePropertie(FontStyle.FontStyleType.Italic);
							break;
						}
						if (attribute.Value == "italic")
							fontStyles.AddPropertie(FontStyle.FontStyleType.Italic);
						else
							DebugProgram.PrintString($"ERROR : unknown italic style : '{attribute.Value}'.");
						break;
					case "text-underline-style":
						if (attribute.Value == "none") {
							fontStyles.RemovePropertie(FontStyle.FontStyleType.Underlined);
							break;
						}
						fontStyles.AddPropertie(FontStyle.FontStyleType.Underlined);
						break;
					case "text-line-through-style":
						if (attribute.Value == "none") {
							fontStyles.RemovePropertie(FontStyle.FontStyleType.Linestrike);
							break;
						}
						fontStyles.AddPropertie(FontStyle.FontStyleType.Linestrike);
						break;
					// graphic-properties -> IGNORE(shadows*,stroke*,protect)
					case "fill":
					case "fill-color":
						if (fillDone)
							break;
						fill = new Fill(element);
						fillDone = true;
						break;
					case "stroke": // type
						if (stroke == null) // cas 1 : rien pour le moment, on créer un nouveau avec juste le type
							stroke = Fill.FromType(attribute.Value);
						else // cas 2 : stroke existant, on mute avec le type.
							stroke = stroke.MutateWithType(attribute.Value);
					//	DebugProgram.PrintString(Name + "| type (" + attribute.Value + ") changed > " + stroke);
						break;
					case "stroke-color":
						if (stroke == null) // cas 1 : rien pour le moment, on créer un nouveau avec juste la la ouleur
							stroke = new Fill(attribute.Value);
						else // cas 2 : stroke existant, on mute avec la couleur.
							stroke = stroke.MutateWithColor(attribute.Value);
					//	DebugProgram.PrintString(Name+"| color ("+attribute.Value+") changed > " + stroke);
						break;
					case "stroke-width":
						strokeWidth = Size.ParseString(attribute.Value);
						break;
					case "min-height":
						minHeight = Size.ParseString(attribute.Value);
						break;
					case "textarea-vertical-align":
						areaVerticalAlign = attribute.Value;
						break;
					case "color":
						fontColor = attribute.Value;
						break;
					// Padding
					case "padding-top":
						padding.SetSize(SquareSize.SquareSizeType.Top, Size.ParseString(attribute.Value));
						break;
					case "padding-right":
						padding.SetSize(SquareSize.SquareSizeType.Right, Size.ParseString(attribute.Value));
						break;
					case "padding-bottom":
						padding.SetSize(SquareSize.SquareSizeType.Bottom, Size.ParseString(attribute.Value));
						break;
					case "padding-left":
						padding.SetSize(SquareSize.SquareSizeType.Left, Size.ParseString(attribute.Value));
						break;
					// Autre
					case "fill-image-name":
						fillImage = context.GetTextureWithName(attribute.Value);
						if (fillImage == null)
							throw new Exception("new Style > Add element > fill-image-name > this texture (" + attribute.Value + ") doesn't exist !");
						break;
					case "shadow":
					case "shadow-offset-x":
					case "shadow-offset-y":
					case "shadow-color":
					case "marker-start":
					case "marker-start-width":
					case "marker-end":
					case "marker-end-width":
					case "show-unit":
					case "auto-grow-height":
						// Ignoré
						break;
					default:
						DebugProgram.Debug("! Unknown style elem : [" + name+"("+attribute.Value+")]");
						break;
				}
			}
		}

		public override bool HasParent() {
			return parent != null;
		}

		public override IStyle GetParentStyle() {
			if (parent == null)
				throw new Exception("Tryied to access to parent of " + this + ", but no parent has been initialized.");
			return parent;
		}
		
		public override string Name { get; }

		public override StyleType Type { get; }

		public override IStyleList GetStyleList() {
			return listStyle;
		}
	}
}
