﻿using System.Collections.Generic;
using System.Linq;
using ODP.API;

namespace ODP.Generics {
	public class StyleLibrairie : IStylesLibrairie {

		private IList<IStyle> styles;
		private IDictionary<string, IMask> masks;
		private IDictionary<string, ITexture> textures;

		public StyleLibrairie() {
			styles = new List<IStyle>();
			masks = new Dictionary<string, IMask>();
			textures = new Dictionary<string, ITexture>();
		}

		// Styles

		public int Count() {
			return styles.Count;
		}

		public IStyle GetWithName(string name) {
			if (name == null)
				return null;
			foreach (IStyle style in styles)
				if (style.Name == name)
					return style;
			return null;
		}

		public void RegisterStyle(IStyle style) {
			if (!styles.Contains(style)) {
				while(style.HasParent()) {
					DebugProgram.Debug("Derivate ("+style.Name+") with his parent ("+style.GetParentStyle().Name+").");
					style = style.DerivateWithParent(style.GetParentStyle());
				}
				styles.Add(style);
			}
		}

		public IEnumerable<IStyle> Enumerate() {
			return styles.AsEnumerable();
		}

		// Masques

		public void RegisterMask(string name, IMask mask) {
			if (!masks.ContainsKey(name))
				masks.Add(name, mask);
		}

		public IMask GetMaskWithName(string maskName) {
			if (!masks.ContainsKey(maskName))
				return null;
			return masks[maskName];
		}

		// Textures

		public void RegisterTexture(ITexture texture) {
			if (!masks.ContainsKey(texture.Name))
				textures.Add(texture.Name, texture);
		}

		public ITexture GetTextureWithName(string textureName) {
			if (!textures.ContainsKey(textureName))
				return null;
			return textures[textureName];
		}

		private int page = 0;
		public int GetContextualPage() {
			return page;
		}
		public void IncrementContextualPage() {
			page++;
		}
	}
}
