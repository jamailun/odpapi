﻿using System.Collections.Generic;

namespace ODP.API {
	/**
	 *			Définit un style d'indentation. Le #GetItem(depth) renvoie le style d'indentation pour le niveau
	 *		d'indentation précis.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IStyleList : APINameable {
		
		/// <summary>
		/// Renvoie le style d'un item pour une certaine profondeur.
		/// </summary>
		/// <param name="depth">La profondeur du style d'indentation à obtenir.</param>
		/// <returns>null si la prondeur n'est pas définie dans le style.</returns>
		IStyleListItem GetItem(int depth);

		/// <summary>
		/// Renvoie la totalité des éléments dans la liste.
		/// </summary>
		IList<IStyleListItem> GetAllItems();

	}
}
