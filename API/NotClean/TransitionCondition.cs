﻿// Utilisé dans les séquences de transitions de l'API.
// J'ai un peu honte d'avoir fait ça comme ça, mais je ne souhaitais pas que l'utilisateur de l'APi ait à importer 47 trucs pour faire une seule action...
namespace ODP.API {
	public struct TransitionCondition {

		// Condition to do the transition.
		public TransitionConditionType Type;

		// Only used if Type == WaitMillis. Pas très POO je sais je sais :(
		public float MillisParam;

	}
}
