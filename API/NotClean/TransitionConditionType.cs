﻿namespace ODP.API {
	// Used in the TransitionCondition's struct.
	public enum TransitionConditionType {
		None,
		WaitMillis, // Add float as parameter
		WaitClick
	}
}
