﻿using ODP.Transitions;

namespace ODP.API {
	/**
	 *			Gère les transitions associées à une slide.
	 *		Si la slide effectue une transition en entrée, ce sera dans #AnimationOnStart.
	 *		Pour les transitions internes qui suivront (suivant un clic ou une attente) il faut se référer
	 *		à la séquence de transitions (#Sequence)
	 *		
	 *		Auteur : Timothé Rosaz, 2021.
	*/
	public interface ITransitionsTimingRoot {

		/// <summary>
		/// Renvoie la transition à effectuer sur la slide à elle même durant l'apparition.
		/// </summary>
		ITransitionNode AnimationOnStart { get; }

		/// <summary>
		/// Renvoie la séquence en format XML. Déprécié, car Sequence est là pour être utilisé à la place.
		/// </summary>
		/// <seealso cref="Sequence"/>	
		[System.Obsolete("Il est conseillé d'utiliser #Sequence à la place.", false)]
		TransitionsSequenceXml RawSequence { get; }

		/// <summary>
		/// Renvoie la séquence de transitions à effectuer DANS la slide, une fois que celle-ci est apparue.
		/// </summary>
		ITransitionsSequence Sequence { get; }

	}
}
