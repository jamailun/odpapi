﻿using System.Collections.Generic;

namespace ODP.API {

	/**
	 *			Interface qui représente une ligne de texte. Elle n'a pas de style propre car chaque mot de la ligne peut
	 *		avoir un style différent.
	 *			Les notions de 'LineItem' réfère à avoir ou non un "-" ou un rond avant la ligne, ainsi qu'un alinéa.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IPLine : APIXmlUnique, APIStyleable {
		
		/// <summary>
		/// Renvoie un IStyleItem qui précède la ligne, ou null si il n'en existe pas.
		/// </summary>
		IStyleListItem LineItem { get; }
		
		/// <summary>
		/// Renvoie vrai ssi la ligne a un item de liste qui la précède.
		/// </summary>
		bool HasLineItem();
		
		/// <summary>
		/// Renvoie un énumérateur des span de la ligne.
		/// </summary>
		IEnumerable<ISpan> Enumerate();

	}
}
