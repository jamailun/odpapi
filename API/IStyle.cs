﻿using ODP.Utils;

namespace ODP.API {
	/**
	 *			Un style est un ensemble de propriétés qui définissent l'apparence des divers objets dans un diaporama.
	 *		Un style peut posséder un style parent, et peut à tout moment être "dérivé" avec un autre style, en tant que
	 *		parent ou enfant.
	 *			Parmi les propriétés, on trouve aussi bien le nom de la police ou la couleur d'un texte. On trouve aussi les IStyleList
	 *		qui définissent les styles d'indentation.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IStyle : APINameable {
		
	// Styles

		/// <summary>
		/// Renvoie le type associé au style.
		/// </summary>
		StyleType Type { get; }     // NOTE : Il y a moyen que cela soit plus utile !

		/// <summary>
		/// Renvoie la 'IStyleList' associée à ce style, si elle existe. Null sinon.
		/// </summary>
		IStyleList GetStyleList();

	// Héritage

		/// <summary>
		/// Renvoie si oui ou non le style possède un parent.
		/// </summary>
		bool HasParent();

		/// <summary>
		/// Renvoie le style parent à celui-ci. Null si aucun parent n'existe.
		/// </summary>
		IStyle GetParentStyle();

		/// <summary>
		/// Dérive le style en cours avec un style, ce dernier étant considéré comme son ENFANT.
		/// Autrement dit, ce sera les valeurs de l'enfant en priorité, celles du parent sinon.
		/// </summary>
		IStyle DerivateWithChild(IStyle child);
		/// <summary>
		/// Dérive le style en cours avec un style, ce dernier étant considéré comme son ENFANT.
		/// Autrement dit, ce sera les valeurs de l'enfant en priorité, celles du parent sinon.
		/// </summary>
		IStyle DerivateWithParent(IStyle parent);


	// Données du style. Chaque getter est associé avec une méthode pour tester si la donnée est définie.

		/// <summary>
		/// Renvoie la taille de police utilisée
		/// </summary>
		int GetFontSize();
		bool HasFontSize();

		/// <summary>
		/// Renvoie le style (gras, italique,...) de la police utilisée
		/// </summary>
		FontStyle GetFontStyle();
		bool HasFontStyle();


		/// <summary>
		/// Renvoie la couleur de la police utilisée
		/// </summary>
		string GetFontColor();
		bool HasFontColor();

		/// <summary>
		/// Renvoie le nom de la police utilisée
		/// </summary>
		string GetFontName();
		bool HasFontName();

		/// <summary>
		/// Renvoie le mode d'alignement vertical
		/// </summary>
		string GetAreaVerticalAlign();
		bool HasAreaVerticalAlign();

		/// <summary>
		/// Renvoie le mode d'alignement horizontal
		/// </summary>
		string GetTextAlign();
		bool HasTextAlign();

		/// <summary>
		/// Renvoie l'espace d'indentation : l'espace entre la bordure gauche et le début du texte.
		/// </summary>
		Size GetTextIndent();
		bool HasTextIndent();


		/// <summary>
		/// Renvoie le 'margin' du texte.
		/// </summary>
		SquareSize GetMargin();
		bool HasMarginData();

		/// <summary>
		/// Renvoie le 'padding' du texte.
		/// </summary>
		SquareSize GetPadding();
		bool HasPaddingData();

		/// <summary>
		/// Renvoie la hauteur maximale de la ligne de texte
		/// </summary>
		Size GetLineHeight();
		bool HasLineHeight();

		/// <summary>
		/// Renvoie la hauteur minimale de la ligne de texte
		/// </summary>
		Size GetMinHeight();
		bool HasMinHeight();
		
		/// <summary>
		/// Renvoie le remplissement utilisé (texte, image, couleur, ...)
		/// </summary>
		Fill GetFill();
		bool HasFillData();
		
		/// <summary>
		/// Renvoie le mode de remplissement du 'stroke'
		/// </summary>
		Fill GetStroke();
		bool HasStrokeData();

		/// <summary>
		/// Renvoie l'épaisseur du 'stroke'
		/// </summary>
		Size GetStrokeWidth();
		bool HasStrokeWidth();


		/// <summary>
		/// Renvoie l'image de fond. Utilisée par les types graphiques (même si rien ne permet de faire un héritage en fonction du type de style)
		/// </summary>
		ITexture GetFillImage();
		bool HasFillImage();
	}
}
