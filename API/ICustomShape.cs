﻿using System.Collections.Generic;
namespace ODP.API {
	/**
	 *			Une CostomShape contient une forme SVG. L'implémentation de ODPAPI devra ensuite interpréter les chaines SVG
	 *		comme bon lui semblera.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ICustomShape : ISlideElement {
		
		/// <summary>
		/// Dictionnaire des attributs (ou plutot des paramètres).
		/// Dans le cas où la formule présente des symboles du style "?f1", ces derniers seront ici associé à un string de définition.
		/// </summary>
		IDictionary<string, string> Arguments();
		
		/// <summary>
		/// Renvoie true si la shape présente un paragraphe en son sein.
		/// </summary>
		bool HasText();

		//TODO: ne pas faire juste la première ligne... mais tout le paragraphe.
		
		/// <summary>
		/// Renvoie le parapgraphe présent dans la forme si il existe, null sinon.
		/// </summary>
		IPLine GetText();
		
		/// <summary>
		/// Renvoie le type de forme défini dans le xml. Peu utile avec un vrai interpréteur.
		/// </summary>
		string ShapeType { get; }

		/// <summary>
		/// Renvoie la 'viewbox', au format : "x y w h", les valeurs par défault sont "0 0 21600 21600"
		/// </summary>
		string ViewBox { get; }

		/// <summary>
		/// Renvoie le path de la shape. J'ai trouvé de la doc ici : https://www.w3.org/TR/SVG/paths.html
		/// </summary>
		string EnhancedPath { get; }
		
		/// <summary>
		/// Renvoie la balise entière.
		/// </summary>
		string GetRawSvg();

	}
}
