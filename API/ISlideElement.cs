﻿namespace ODP.API {
	/**
	 *			Désigne un élément contenu dans une IFrame.
	 *		Ne possède pas de propriété en dehors de son ID. L'implémentation s'occupe de savoir de quel
	 *		sous genre il s'agit.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ISlideElement : APIXmlUnique {}

}
