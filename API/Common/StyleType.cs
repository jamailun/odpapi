﻿namespace ODP.API {
	/**
	*		Permet de différencier les styles.
	*		Je ne me suis jamais servi de ceci, il serait bien de s'en servir ça évitera pas mal de problèmes.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public enum StyleType {
		// used when style is created from StyleList
		TextList,

		// only a <slide> can use it
		DrawingPage,

		// only in <frames> (sometimes) or <page-thumbnail> (in <notes>)
		Graphic,

		// only used by <frames> : pretty usefull
		Presentation,

		// used in <frames text-style-name>. Apparently defines text style in frames.
		Paragraph,

		// used by textes
		Text
	}
}
