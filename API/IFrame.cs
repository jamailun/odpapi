﻿namespace ODP.API {
	/**
	 *			Cette interface est en gros on container. On peut récupérer son contenu avec #GetContent().
	 *		Il permet de positionner son contenu. Ensuite, pour savoir quel est le type du contenu, c'est
	 *		l'implémentation de l'API qui gère.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IFrame : APILayoutable, APIRectangle, APIStyleable, APIRotable, APIXmlUnique {
		
		/// <summary>
		/// Un getter pour récupérer le nom du style associé
		/// </summary>
		string TextStyleName { get; }
		
		/// <summary>
		/// Renvoie l'élément contenu dans la frame
		/// </summary>
		ISlideElement GetContent();
		
		/// <summary>
		/// Renvoie la classe de présentation (pas utilisée pour le moment)
		/// </summary>
		string PresentationClass { get; }

	}
}
