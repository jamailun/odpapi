﻿using System.Collections.Generic;

namespace ODP.API {
	/**
	 *			Définit une bibliothèque permettant de récupérer des Styles, des Textures et des Mask
	 *		à l'aide de leur nom.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IStylesLibrairie {

		// Masque

		/// <summary>
		/// Obtenir un masque à partir de son nom. Renvoie null si nom inconnu.
		/// </summary>
		/// <param name="maskName">Nom du masque à renvoyer</param>
		IMask GetMaskWithName(string maskName);
		// Interne... Enregistrer un masque...
		void RegisterMask(string name, IMask mask);
		
		// Texture

		/// <summary>
		/// Obtenir une texture à partir de son nom. Renvoie null si nom inconnu.
		/// </summary>
		/// <param name="textureName">Nom de la texture à renvoyer.</param>
		ITexture GetTextureWithName(string textureName);
		// Interne... Enregistrer une texture...
		void RegisterTexture(ITexture texture);
		
		/// <summary>
		/// Renvoie le nombre de styles qui existent.
		/// </summary>
		int Count();

		/// <summary>
		/// Revoie un enumerable de tous les styles enregistrés
		/// </summary>
		IEnumerable<IStyle> Enumerate();
		// Récupérer un style précis.

		/// <summary>
		/// Obtenir un style à partir de son nom. Renvoie null si nom inconnu.
		/// </summary>
		/// <param name="name">Nom du style à renvoyer.</param>
		IStyle GetWithName(string name);

		// Sauvegarder un nouveau style. Interne :(
		void RegisterStyle(IStyle style);

		// contextual slide page. Utilisés pour savoir où on en est dans les <number>.
		int GetContextualPage();
		void IncrementContextualPage();

	}
}
