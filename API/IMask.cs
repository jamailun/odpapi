﻿using System.Collections.Generic;

namespace ODP.API {
	/**
	 *			Un masque est un ensemble de frame à afficher en arrière plan de la slide.
	 *		Ce sont les slides qui ont une collection de masques auxquelles elles sont liées.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IMask : APINameable {
		
		/// <summary>
		/// Renvoie le Style de la page associé
		/// </summary>
		IStyle PageStyle { get; }
		
		/// <summary>
		/// Renvoie un enumerable des frames contenues dans le masque.
		/// </summary>
		IEnumerable<IFrame> GetFrames();

	}
}
