﻿using System.Xml.Linq;
using ODP.Utils;

namespace ODP.API {
	/**
	 * A REWORK : n'est pas open-closed friendly !!
	 * 
	 *		Définit un style d'indentation pour un niveau précis.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface IStyleListItem {
		
		/// <summary>
		/// Add <list-level-properties> element
		/// </summary>
		void AddListLevelPropertiesElement(XElement element);
		
		/// <summary>
		/// Add <text-properties> element
		/// </summary>
		void AddTextPropertiesElement(XElement element);
		
		/// <summary>
		/// Get TextLevel (line number of this bullet)
		/// </summary>
		int GetTextLevel();
		
		/// <summary>
		/// Get string corresponding to the bullet
		/// </summary>
		string GetBulletChar();
		
		/// <summary>
		/// Get minimum label width of bullet
		/// </summary>
		Size GetMinLabelWidth();
		
		/// <summary>
		/// Get space before a bullet
		/// </summary>
		Size GetSpaceBefore();
		
		/// <summary>
		/// Get font size used for the bullet
		/// </summary>
		Size GetFontSize();
		
		/// <summary>
		/// Get font family used for the bullet.
		/// </summary>
		string GetFontFamily();
		
		/// <summary>
		/// Returns true if uses basic font color for the bullet.
		/// </summary>
		bool UseWindowFontColor();
	}
}
