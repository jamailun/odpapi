﻿namespace ODP.API {
	/**
	*		Permet de répérer dans le temps un objet dans une slide.
	*			Utilisé dans les transitions.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APITimeable {
		
		/// <summary>
		/// Renvoie la durée de l'évènement
		/// </summary>
		float Duration { get; }
		
		/// <summary>
		/// Renvoie le temps (en secondes) à partir duquel l'évènement PEUT être réalisé.
		/// </summary>
		float BeginTime { get; }

	}
}
