﻿namespace ODP.API {
	/**
	*		Rassemble simplement une position et une dimension en une structure de données.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIRectangle : APIDimensionable, APIPositionable {}
}
