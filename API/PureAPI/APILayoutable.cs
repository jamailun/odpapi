﻿namespace ODP.API {
	/**
		*		Permet de positionner un objet dans un 'layout'
		*		Ceci n'est pas encore utilisé pour le moment, mais pourrait l'être dans une implémentation.
		*		
		*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APILayoutable {
		/// <summary>
		/// Renoie le nom du layout de cet élément.
		/// </summary>
		string LayoutName { get; }
	}
}
