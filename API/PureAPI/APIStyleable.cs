﻿namespace ODP.API {
	/**
	*		Permet de donner un style à un objet dans une slide.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIStyleable {
		/// <summary>
		/// Renvoie le style associé à cet élément.
		/// </summary>
		IStyle Style { get; }
	}
}
