﻿using ODP.Utils;
namespace ODP.API {
	/**
	*		Permet de donner une position à un objet dans une slide.
	*		Les coordoonées sont par rapport au coin en haut à gauche.
	*			x = axe horizontal,
	*			y = axe vertical.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIPositionable {

		/// <summary>
		/// Renvoie la position de l'élément sur l'axe horizontal à partir de la gauche.
		/// </summary>
		Size X { get; }

		/// <summary>
		/// Renvoie la position de l'élément sur l'axe vertical à partir du haut.
		/// </summary>
		Size Y { get; }

	}
}
