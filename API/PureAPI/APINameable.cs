﻿namespace ODP.API {
	/**
	*		Permet de donner un nom à un objet d'une slide.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APINameable {
		/// <summary>
		/// Nom de l'élément
		/// </summary>
		string Name { get; }
	}
}
