﻿namespace ODP.API {
	/**
	*		Permet de savoir si une ressource est affichée par défault. Une ressource peut ne pas l'être si une transition
	*	est nécessaire pour l'afficher par exemple.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIVisible {

		/// <summary>
		/// Renvoie true si l'objet est visible dès le début de la slide.
		/// Par exemple, si une image a une transition qui la fait appraitre après un clic, celle-ci ne sera pas visible au début de la slide.
		/// </summary>
		bool VisibleByDefault { get; set; }

	}
}
