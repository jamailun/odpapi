﻿using ODP.Utils;
namespace ODP.API {
	/**
	 *		Permet de donner des dimensions à un objet dans une slide.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIDimensionable {

		/// <summary>
		/// Renvoie la taille de cet élément sur l'axe horizontal.
		/// </summary>
		Size Width { get; }

		/// <summary>
		/// Renvoie la taille de cet élément sur l'axe vertical.
		/// </summary>
		Size Height { get; }

	}
}
