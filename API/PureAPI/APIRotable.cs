﻿using ODP.Utils;
namespace ODP.API {
	/**
	*		Permet de donner une rotation à un objet d'une slide.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIRotable {
		/// <summary>
		/// Renvoie la rotation de cet élément. Notons que ce sera toujours une Size en radians.
		/// </summary>
		Size Rotation { get; }
	}
}
