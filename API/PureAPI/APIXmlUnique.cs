﻿namespace ODP.API {
	/**
	*		Permet de donner un ID à un objet dans une slide.
	*		Ces ID sont utilisés durant les transitions, pour identifier les objet à déplacer.
	*		
	*		Tous les objets n'ont pas d'ID. Si il n'en a pas, le getter #XmlID renverra null.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface APIXmlUnique : APIVisible {

		/// <summary>
		/// Renvoie l'identifiant unique utilisé dans le xml.
		/// Il est utilisé dans les transitions pour identifier l'élément concerné.
		/// </summary>
		string XmlID { get; }

		/// <summary>
		/// Renvoie true si cet élément a un identifiant unique spécifié dans le xml.
		/// </summary>
		bool HasXmlID();
		
		/// <summary>
		/// Renvoie un objet dont l'ID unique xml, parmi les enfants de cet éléments (et lui-même).
		/// </summary>
		/// <param name="id">ID que possède l'élément à récupérer.</param>
		/// <returns>null si aucun élément dans les enfants ne correspond à l'id spécifié.</returns>
		APIXmlUnique GetXmlIdInChildren(string id);
		
	}
}
