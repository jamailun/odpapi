﻿using System.Collections.Generic;

namespace ODP.API {
	/**
	 *			Définit une SUITE de plusieurs transitions.
	 *		Le fonctionnement est celui d'une machine à noeuds. à tout moment, cet objet possède une condition pour
	 *		passer à l'étape suivante. On peut alors signaler que la condition a été validée (l'implémentation de l'API
	 *		s'en occupe) et passer à la transition suivante.
	 *		
	 *		Notons que si 2 séquences sont dans un même groupes, elles sont simultanées.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ITransitionsSequence {
		
		/// <summary>
		/// Renvoie le bloc actuels de transitions à effectuer.
		/// </summary>
		/// <returns>Renvoie null si il n'y a palus de bloc.</returns>
		IEnumerable<ITransitionNode> GetCurrentTransitionsBlock();
		
		/// <summary>
		/// Renvoie la condition pour passer au boc suivant.
		/// </summary>
		/// <returns>Returns null if no more block.</returns>
		TransitionCondition GetCurrentCondition();
		
		/// <summary>
		/// Renvoie true si il reste un bloc après celui-ci.
		/// </summary>
		bool HasMore();
		
		/// <summary>
		/// A appeler une fois que la condition est remplie, passe au bloc suivant.
		/// </summary>
		/// <returns>Renvoie true si il reste un bloc ensuite, false sinon.</returns>
		bool ConditionAccepted();
		
		/// <summary>
		/// A appeller pourretourner au bloc précédent dans la séquence.
		/// Renvoie true si c'est un succès, renvoie false si ce bloc était le premier.
		/// </summary>
		bool ForcePrevious();

	}
}
