﻿using System.IO;

namespace ODP.API {
	/**
	 *			Une texture est une image contenue par une image.
	 *		L'image peut être récupérée avec la méthode #GetImage() qui renvoie un MemoryStream.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ITexture : ISlideElement, APINameable {

		/// <summary>
		/// Getter pour le nom affiché de la texture.
		/// </summary>
		string DisplayName { get; }

		/// <summary>
		/// Chemin relatif dans l'archive pour retrouver l'image.
		/// </summary>
		string ImagePath { get; }

		/// <summary>
		/// Renvoie un MemoryStream (https://docs.microsoft.com/fr-fr/dotnet/api/system.io.memorystream?view=net-5.0) contenant la texture.
		/// Utilisable par Unity pour créer des Textures2D.
		/// </summary>
		MemoryStream GetImage();

	}
}
