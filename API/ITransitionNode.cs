﻿namespace ODP.API {
	/**
	 *			Cible un xlmID de précis pour effectuer des actions ensuite.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ITransitionNode : APITimeable {

		/// <summary>
		/// Renvoie l'xmlID concerné par la transition.
		/// </summary>
		string TargetElementID { get; }

	}
}
