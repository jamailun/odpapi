﻿namespace ODP.API {
	/**
	 *			Groupe de mot contenu dans une IPLine.
	 *		Possède un style et une valeur textuelle.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ISpan : APIStyleable, APIXmlUnique {
		
		/// <summary>
		/// Renvoie vrai si le span possède un style propre
		/// </summary>
		bool HasStyle();
		
		/// <summary>
		/// Renvoie le contenu textuel du span.
		/// </summary>
		string Text { get; }

	}
}
