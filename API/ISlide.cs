﻿using System.Collections.Generic;

namespace ODP.API {
	/**
	 *			Cette interface joue le rôle de Slide. C'est le même fonctionnement qu'une vraie slide :
	 *		elle contient plusieurs 'frames' qui contiennent soit du texte, soit une image, soit une forme...
	 *			C'est aussi le conteneur des transitions internes (obtenable avec #TimingRoot).
	 *			Enfin, on peut réupérer le masque (les images / textes présents sur l'ensemble des slides en arrière plan) avec #Mask.
	 *		
	 *		Auteur : Timothé ROSAZ, 2021.
	*/
	public interface ISlide : APILayoutable, APINameable, APIStyleable, APIXmlUnique {

		/// <summary>
		/// Récupérer le numéro de la slide actuelle. Commence à 1.
		/// </summary>
		int Position { get; }
		
		/// <summary>
		/// Récupérer tous les éléments de la slide
		/// </summary>
		IList<IFrame> GetElements();
		
		/// <summary>
		/// Nom de la page maitresse. Utilité non trouvée pour le moment.
		/// </summary>
		string MasterPageName { get; }
		
		/// <summary>
		/// Renvoie la  racine des transitions de la diapositive, si elle existe.
		/// </summary>
		ITransitionsTimingRoot TimingRoot { get; }
		
		/// <summary>
		/// Un getter pour récupérer le masque de la diapositive
		/// </summary>
		IMask Mask { get; }
	}

}
