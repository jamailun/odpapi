using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using ODP.API;
using ODP.Generics;

namespace ODP {
	/**
	*		Classe interne qui a plusieurs buts utilitaires lors de la génération du ODPDocument.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public class XmlParser {

		private static IList<string> STYLE_IGNORED = new List<string>() {
			// A NE PLUS IGNORER ABSOLUMENT
			"date-style",
			"gradient", "stroke-dash", //( <- SVG )
			// mode édition uniquement
			"marker", "default-style", "presentation-page-layout", "handout-master", "page-layout",
			// à lire éventuellement plus tard
			"layer-set"
		};

		private readonly XDocument doc;
		private static ZipArchive zip;

		/// <summary>
		/// Créer une instance de XmlParser, servant à transformer un string de xml de ODP.
		/// </summary>
		/// <see cref="ParseXml"/>
		/// <param name="value">Valeur du xml à parser</param>
		/// <param name="zip">Lie à l'archive en train d'être lue. Requise pour</param>
		public XmlParser(string value, ZipArchive zip) {
			doc = XDocument.Parse(value);
			XmlParser.zip = zip;
		}

		private XElement GetRoot() {
			return doc.Root;
		}

		public ODPDocument ParseXml() {
			return ParseXml(false);
		}

		public ODPDocument ParseXml(bool debug) {
			return ParseXml(new ODPDocument().GetStylesLibrairie(), debug);
		}

		public ODPDocument ParseXml(IStylesLibrairie styles) {
			return ParseXml(styles, false);
		}

		public ODPDocument ParseXml(IStylesLibrairie styles, bool debug) {
			ParseStyles("styles", styles, debug);
			ParseStyles("automatic-styles", styles, debug);
			ParseStyles("master-styles", styles, debug);
			IList<ISlide> slides = ParseSlides(styles, debug);
			return new ODPDocument(slides, styles);
		}

		// Remplit la bibliothèque des styles donnée en paramètre.
		private void ParseStyles(string sectorName, IStylesLibrairie styles, bool debug) {
			XElement autoStyleE = GetElementWithName(GetRoot(), sectorName);
			if (autoStyleE == null)
				return;
			foreach (XElement element in GetChildren(autoStyleE)) {
				string name = element.Name.LocalName;
				if (STYLE_IGNORED.Contains(name))
					continue;

				if(name == "fill-image") {
					ITexture texture = new Texture(element);
					styles.RegisterTexture(texture);
					DebugProgram.Debug("~+ Texture");
					continue;
				}

				if(name == "style") {
					IStyle style = new Style(styles, element);
					styles.RegisterStyle(style);
					continue;
				}

				if(name == "list-style") {
					IStyleList styleList = new StyleList(element);
					styles.RegisterStyle( new Style( styleList ));
					continue;
				}

				if(name == "master-page") {
					IMask mask = new Mask(element, styles);
					styles.RegisterMask(mask.Name, mask);
					continue;
				}

				throw new Exception("ParseStyles > Unknown STYLE tag : " + name + ".");
			}
		}

		// Remplit la collection de slides de l'instance.
		private IList<ISlide> ParseSlides(IStylesLibrairie styles, bool debug) {
			IList<ISlide> slides = new List<ISlide>();
			XElement bodyE = GetElementWithName(GetRoot(), "body");
			if (bodyE == null)
				return slides;
			foreach (XElement element in GetChildren(GetElementWithName(bodyE, "presentation"))) {
				string name = element.Name.LocalName;
				if (name == "settings")
					continue;
				if(name == "page") {
					styles.IncrementContextualPage();
					ISlide slide = new Slide(styles, element, (slides.Count + 1));
					slides.Add(slide);
					continue;
				}
				throw new Exception("ParseStyles > Unknown STYLE tag : " + name + ".");
			}
			return slides;
		}
		
		// Permet de récupérer un enfant d'un noeud.
		private static XElement GetElementWithName(XElement root, string name) {
			XNode node = root.FirstNode;
			do {
				if (!(node is XElement))
					continue;
				XElement element = (XElement)node;
				if (element.Name.LocalName == name)
					return element;
			} while ((node = node.NextNode) != null);
			return null;
		}

		// Méthode utilitaire pour récupérer les enfants d'une node.
		internal static IList<XElement> GetChildren(XElement elementParent) {
			IList<XElement> list = new List<XElement>();
			if (elementParent == null)
				return list;
			XNode node = elementParent.FirstNode;
			if (node == null)
				return list;
			do {
				if (node is XElement)
					list.Add((XElement)node);
			} while ((node = node.NextNode) != null);
			return list;
		}

		// Méthode utilitaire appelée un peu partout : obtient la valeur d'un attribut et renvoie une erreur si jamais l'attribut n'existe pas.
		internal static string GetAttributeValue(XElement element, string attrName) {
			foreach ( XAttribute xAttribute in element.Attributes() ) {
				if ( xAttribute.Name.LocalName == attrName )
					return xAttribute.Value;
			}
			throw new Exception("ERROR ! no (" + attrName + ") attribute in element [" + element.Name.LocalName + "].");
		}

		// Idem que précédemment, mais renvoie null à la place de throw une erreur en cas d'attribut absent.
		public static string GetAttributeValueNullable(XElement element, string attrName) {
			foreach (XAttribute xAttribute in element.Attributes()) {
				if (xAttribute.Name.LocalName == attrName)
					return xAttribute.Value;
			}
			return null;
		}

		// Transformation d'une ZipArchiveEntry internet en image sous la forme d'un MemoryStream.
		internal static MemoryStream ReadImageFromODP(string entryName) {
			if (zip == null)
				throw new Exception("XmlParser opened without zip specified.");
			ZipArchiveEntry zipEntry = zip.GetEntry(entryName);
			if (zipEntry == null)
				throw new Exception("ERROR : could not read /" + entryName + " of ODP file.");

			var zipStream = zipEntry.Open();
			var memoryStream = new MemoryStream();

			zipStream.CopyTo(memoryStream);
			memoryStream.Position = 0;

			return memoryStream;
		}
	}
}
