﻿using System.Collections.Generic;
using System.Linq;
using ODP.API;
using ODP.Generics;

namespace ODP {
	/**
	*		Classe générée par l'API, ayant pour but d'être utilisée par l'implémentation de celle-ci.
	*		Stocke les slides et la bibliothèque de styles.
	*		Les slides peuvent être récupérées individuellement (par leur numéro) ou on peut obtenir un Iterable pour tout obtenir d'un coup.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public sealed class ODPDocument {

		private IList<ISlide> slides;
		private IStylesLibrairie styles;

		internal ODPDocument() {
			slides = new List<ISlide>();
			styles = new StyleLibrairie();
		}

		internal ODPDocument(IList<ISlide> slides, IStylesLibrairie styles) {
			this.slides = slides;
			this.styles = styles;
		}

		internal void CombineSlides(ODPDocument document) {
			foreach (ISlide slide in document.slides)
				this.slides.Add(slide);
		}

		// Return a COPY of slides list. Public access.
		public IList<ISlide> GetSlidesList() {
			return new List<ISlide>(slides);
		}

		internal IStylesLibrairie GetStylesLibrairie() {
			return styles;
		}

		public int GetSlidesCount() {
			return slides.Count;
		}

		public ISlide GetSlideNumber(int number) {
			foreach (ISlide slide in slides)
				if (slide.Position == number)
					return slide;
			return null;
		}

		public IStyle GetStyle(string styleName) {
			return styles.GetWithName(styleName);
		}

		public IEnumerable<ISlide> EnumerateSlides() {
			return slides.AsEnumerable();
		}

		public void Print() {
			DebugProgram.PrintString("All data in ODP document :\n");
			DebugProgram.PrintString("> STYLES : ("+styles.Count()+")\n");
			foreach (IStyle style in styles.Enumerate()) {
				DebugProgram.PrintString(style + "\n");
			}
			DebugProgram.PrintString("\n> SLIDES ("+slides.Count+") :\n");
			foreach (ISlide sl in slides) {
				DebugProgram.PrintString(sl + "\n");
			}
		}

	}
}
