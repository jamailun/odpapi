﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;

namespace ODP {
	/**
	*		Classe d'accès à l'API.
	*		
	*		Permet de générer un ODPDocument qui contiendra les données de la slide.
	*		Il faut fournir le chemin d'accès au fichier.
	*		
	*		Auteur : Timothé ROSAZ, 2021.
	*/
	public class ODPParser {

		private static readonly IList<string> ODP_ENTRIES = new List<string>() { "styles.xml", "content.xml" };

		public static ODPDocument ParseODPFile(string odpPath) {
			return ParseODPFile(odpPath, false);
		}

		public static ODPDocument ParseODPFile(string odpPath, bool debug) {
			// Ouverture du .odp
			ZipArchive odpArchive = ZipFile.Open(odpPath, ZipArchiveMode.Read);

			// Récupération du fichier dans l'archive (celui qui contient le xml à parser ensuite)
			ODPDocument odp = new ODPDocument();

			foreach (string entry in ODP_ENTRIES) {

				// On récupère l'entry voulue.
				ZipArchiveEntry zipEntry = odpArchive.GetEntry(entry);
				if (zipEntry == null)
					throw new Exception("ERROR : could not read /"+entry+" of ODP file.");
				string xmlValue = new StreamReader(zipEntry.Open()).ReadToEnd();

				if (debug)
					DebugProgram.Debug("Entering entry /" + entry + ".");

				// On créer un parser avec la valeur de l'entry.
				XmlParser parser = new XmlParser(xmlValue, odpArchive);

				// On fait un ODPDocument avec l'instance de la StyleLibrairie du doc de retour.
				ODPDocument outDoc = parser.ParseXml(odp.GetStylesLibrairie(), true);

				// On rajoute nos slides dans le vrai doc. Les styles sont déjà ajouté par partage d'instance.
				odp.CombineSlides(outDoc);
			}

			return odp;
		}
	}
}
