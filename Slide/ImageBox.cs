﻿using System.Xml.Linq;
using System.Collections.Generic;
using ODP.API;
using System.IO;

namespace ODP.Generics {
	public class ImageBox : ISlideElement {

		private readonly IStylesLibrairie context;
		private readonly IList<IPLine> paragraph;
		bool _visibleByDefault = true;
		public bool VisibleByDefault {
			get {
				if (! _visibleByDefault)
					return false;
				foreach (IPLine l in paragraph) {
					if (!l.VisibleByDefault)
						return false;
				}
				return true;
			}
			set { _visibleByDefault = value; }
		}

		public ImageBox(IStylesLibrairie context, XElement source, IStyle parentStyle) {
			this.context = context;
			paragraph = new List<IPLine>();
			XmlID = XmlParser.GetAttributeValueNullable(source, "id");

			foreach (XElement child in XmlParser.GetChildren(source)) {
				// Ajout de texte interne si besoin
				switch (child.Name.LocalName) {
					case "p":
						paragraph.Add( new PLine(context, child, parentStyle) );
						break;
					default:
						throw new System.Exception("new Image > Unexpected tag name : " + child.Name.LocalName + ".");
				}
			}
			// Lecture des attributs.
			ImagePath = XmlParser.GetAttributeValue(source, "href");
			ShowValue = XmlParser.GetAttributeValue(source, "show");
			TypeValue = XmlParser.GetAttributeValue(source, "type");
		}

		public MemoryStream GetImage() { // on appelle la méthode utilitaire de XmlParser à chaque appel de cette fonction.
			return XmlParser.ReadImageFromODP(ImagePath);
		}

		public string ImagePath { get; }
		public string ShowValue { get; }
		public string TypeValue { get; }

		public string XmlID { get; private set; }

		public IList<IPLine> GetText() {
			return new List<IPLine>(paragraph);
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (HasXmlID() && XmlID == id)
				return this;
			foreach (IPLine line in paragraph) {
				if (line.HasXmlID() && line.XmlID == id)
					return line;
				APIXmlUnique son = line.GetXmlIdInChildren(id);
				if (son != null)
					return son;
			}
			return null;
		}

		public bool HasXmlID() {
			return XmlID != null;
		}

		public override string ToString() {
			return "Image[path=" + ImagePath + ", show=" + ShowValue + ", type=" + TypeValue + 
				(paragraph.Count > 0 ? ", (additionalText:" + paragraph.Count + ")" : "")
				+ "]";
		}
	}
}