﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class CustomShapeFrame : Frame {

		public static readonly List<string> ALLOWED_SHAPE_DATA = new List<string>() { "enhanced-geometry", "p" };
		
		private PLine additionalText;
		private CustomShape shape;

		public CustomShapeFrame(IStylesLibrairie context, XElement source, IStyle slideStyle) : base(context, source, slideStyle) {}

		public override ISlideElement GetContent() {
			// On met le texte au moment de return la shape car on ne sait pas dans quel ordre les choses arrivent.
			return shape.ReturnAndAddText(additionalText);
		}

		protected override void SetContent(XElement element, IStyle style) {
			switch (element.Name.LocalName) {
				case "enhanced-geometry":
					shape = new CustomShape(element);
					break;
				case "p":
					additionalText = new PLine(context, element, style);
					break;
				case "list":
					return;
				default:
					throw new Exception("Unknown frame content : '" + element.Name.LocalName + "'.");
			}
		}

		public string GetRawSvg() {
			return shape.GetRawSvg();
		}
		
	}
}
