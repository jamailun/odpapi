﻿using System.Xml.Linq;
using System.Collections.Generic;
using ODP.API;

namespace ODP.Generics {
	public class CustomShape : ICustomShape {

		private PLine text;
		private string rawSvg;
		public bool VisibleByDefault { get; set; }
		private IDictionary<string, string> arguments = new Dictionary<string, string>();

		public CustomShape(XElement element) {
			ShapeType = XmlParser.GetAttributeValue(element, "type");
			XmlID = XmlParser.GetAttributeValueNullable(element, "id");
			ViewBox = XmlParser.GetAttributeValue(element, "viewBox");
			EnhancedPath = XmlParser.GetAttributeValue(element, "enhanced-path");
			rawSvg = element.ToString();
			VisibleByDefault = true;

			foreach (XElement child in element.Descendants()) {
				if (child.Name.LocalName != "equation")
					continue;
				arguments.Add(XmlParser.GetAttributeValue(child, "name"), XmlParser.GetAttributeValue(child, "formula"));
			}
		}

		public CustomShape ReturnAndAddText(PLine text) {
			this.text = text;
			return this;
		}

		public IDictionary<string, string> Arguments() {
			return new Dictionary<string, string>(arguments);
		}

		public bool HasText() {
			return text != null;
		}

		public IPLine GetText() {
			return text;
		}

		public string ShapeType { get; }

		public string ViewBox { get; }
		public string EnhancedPath { get; }

		public override string ToString() {
			return $"CustomShape[shape={ShapeType},text=({text})]";
		}
		public string XmlID { get; private set; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (XmlID != null && XmlID == id)
				return this;
			return text.GetXmlIdInChildren(id);
		}

		public string GetRawSvg() {
			return rawSvg;
		}
	}
}
