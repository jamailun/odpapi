﻿using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class Span : ISpan {

		public bool VisibleByDefault { get; set; }
		public IStyle Style { get; }
		public string Text { get; }

		public Span(IStylesLibrairie context, XElement element, IStyle parentStyle) {
			Text = element.Value;
			Style = parentStyle.DerivateWithChild(context.GetWithName(XmlParser.GetAttributeValueNullable(element, "style-name")));
			XmlID = XmlParser.GetAttributeValueNullable(element, "id");
			VisibleByDefault = true;
		}

		public Span(string text, IStyle style) {
			Text = text;
			Style = style;
			VisibleByDefault = true;
		}
		
		public bool HasStyle() {
			return Style != null;
		}

		public override string ToString() {
			if (HasStyle())
				return "\"" + Text + "\"(" + Style.Name + ")";
			return Text;
		}

		public string XmlID { get; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			return (XmlID != null && XmlID == id) ? this : null;
		}
	}
}
