﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class PLine : IPLine {

		private IList<ISpan> words = new List<ISpan>();
		public bool VisibleByDefault { get; set; }

		public IStyle Style { get; }

		internal PLine(IStylesLibrairie context, XElement source, IStyle parentStyle) : this(context, source, parentStyle, null) {}

		internal PLine(IStylesLibrairie context, XElement source, IStyle parentStyle, IStyleListItem itemStyle) {
			LineItem = itemStyle;
			XmlID = XmlParser.GetAttributeValueNullable(source, "id");
			VisibleByDefault = true;

			string styleName = XmlParser.GetAttributeValueNullable(source, "style-name");
			Style = (styleName != null) ? parentStyle.DerivateWithChild(context.GetWithName(styleName)) : parentStyle;
			

			// Cas 1 : le <p> contient directement la ligne.
			if (source.Descendants().Count() == 0) {
				words.Add(new Span(source.Value, Style));	
				return;
			}

			// Cas 2 : le <p> possède des <span> enfants ayant chacun leur propre style.
			foreach (XElement child in source.Descendants()) {
				if (child.Name.LocalName != "span")
					continue;
				if (child.HasElements) {
					foreach (XElement subchild in child.Elements()) {
						IStyle pStyle = context.GetWithName(XmlParser.GetAttributeValueNullable(subchild, "style-name"));
						HandleSpecialData(subchild, pStyle ?? Style, context);
					}
				} else {
					words.Add(new Span(context, child, Style));
				}
			}
		}

		private void HandleSpecialData(XElement elem, IStyle style, IStylesLibrairie context) {
			string result;
			switch (elem.Name.LocalName) {
				case "page-number":
					result = elem.Value.Replace("<number>", ""+context.GetContextualPage());
					words.Add(new Span(result, style));
					break;
				case "line-break":
				case "s":
				case "tab":
				case "footer":
				case "date-time":
					// Non développé :(
					break;
				default:
					throw new System.Exception("Unknown special element : ("+elem.Name.LocalName+").");
			}
		}

		public IStyleListItem LineItem { get; }

		public bool HasLineItem() {
			return LineItem != null;
		}

		public IEnumerable<ISpan> Enumerate() {
			return words.AsEnumerable();
		}

		public override string ToString() {
			string str = "PLine[";
			if(Style != null)
				str += "Style="+Style.Name+",";
			bool first = true;
			foreach(ISpan span in words) {
				if (first)
					first = false;
				else
					str += ", ";
				str += span.ToString();
			}
			return str + "]";
		}

		public string XmlID { get; private set; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (XmlID != null && XmlID == id)
				return this;
			foreach (ISpan word in words) {
				if (word.HasXmlID() && word.XmlID == id)
					return word;
				APIXmlUnique son = word.GetXmlIdInChildren(id);
				if (son != null)
					return son;
			}
			return null;
		}

	}
}
