﻿using System;
using System.Xml.Linq;
using ODP.API;
using ODP.Utils;

namespace ODP.Generics {
	public class Frame : IFrame {

		protected readonly IStylesLibrairie context;
		protected ISlideElement content;

		private bool _visibleByDefault = true;
		public bool VisibleByDefault {
			get {
				if ( ! _visibleByDefault)
					return false;
				if(content == null)
					return true;
				return content.VisibleByDefault; // la visibilité de la frame dépend, sauf confition contraire, de la visibilité de son enfant
			}
			set {
				_visibleByDefault = value;
			}
		}

		public Frame(IStylesLibrairie context, XElement source, IStyle slideStyle) {
			this.context = context;
			// Récupération des attributs
			Width = Size.ParseString(XmlParser.GetAttributeValue(source, "width"));
			Height = Size.ParseString(XmlParser.GetAttributeValue(source, "height"));
			X = Size.ParseString(XmlParser.GetAttributeValueNullable(source, "x"));
			Y = Size.ParseString(XmlParser.GetAttributeValueNullable(source, "y"));
			Rotation = new Size(0f, StyleUnit.Radians);
			// Rotation si nécessaire et si possible.
			if (X == null) {
				string translateStr = XmlParser.GetAttributeValueNullable(source, "transform");
				if (translateStr != null) {
					Translate translate = new Translate(translateStr);
					X = translate.TranslationX;
					Y = translate.TranslationY;
					Rotation = translate.Rotation;
				}
			}
			LayoutName = XmlParser.GetAttributeValue(source, "layer");
			XmlID = XmlParser.GetAttributeValueNullable(source, "id");
			TextStyleName = XmlParser.GetAttributeValueNullable(source, "text-style-name");
			PresentationClass = XmlParser.GetAttributeValueNullable(source, "class");
			// On génère un style qui est la fusion de notre balise et du style parent.
			IStyle ownStyle = context.GetWithName(XmlParser.GetAttributeValue(source, "style-name"));
			Style = ownStyle.DerivateWithParent(slideStyle);

			IStyle contentStyle = null;
			if (TextStyleName != null) {
				IStyle contentStyleText = context.GetWithName(TextStyleName);
			//	DebugProgram.PrintString($"New frame layout {LayoutName},\nstyleMixed={Style},\nTEXTStyle={contentStyleText}.");
				contentStyle = Style.DerivateWithParent(contentStyleText);
			}

			foreach (XElement child in XmlParser.GetChildren(source)) { // foreach pour éviter tout risque, mais normallement il ne peut y avoir que 1 node enfant.
				SetContent(child, contentStyle ?? Style);
			}
		}

		public virtual ISlideElement GetContent() {
			return content;
		}

		protected virtual void SetContent(XElement element, IStyle childrenStyle) {
			switch (element.Name.LocalName) {
				case "text-box":
					content = new TextBox(context, element, childrenStyle);
					break;
				case "image":
					content = new ImageBox(context, element, childrenStyle);
					break;
				case "desc":
					// TODO je sais pas trop, ça cahrge une autre image...
					// <svg:desc>S:\serv_com\01_CHARTE-INSA-Rennes\2014\01_LOGOS-ECOLES\LOGO-INSA-RENNES\Formats-PNG-JPG\Logo_INSARennes-quadri.jpg</svg:desc>
					break;
				default: //TODO à commenter pour débuger.
					throw new Exception("new Frame > SetContent > Unexpected tag name : " + element.Name.LocalName + ".");
			}
		}

		public string TextStyleName { get; }

		public string PresentationClass { get; }

		public string LayoutName { get; }

		public Size Rotation { get; }

		public Size Width { get; }

		public Size Height { get; }

		public Size X { get; }

		public Size Y { get; }

		public IStyle Style { get; }

		public override string ToString() {
			string str = this.GetType().Name + "[";
			str += "LayoutName='" + LayoutName + "',";
			if (PresentationClass != null)
				str += "PresentationClass='" + PresentationClass + "',";
			if (TextStyleName != null)
				str += "TextStyleName='" + TextStyleName + "',";
			str += "Style='" + Style.Name + "',";
			str += "Width='" + Width + "',";
			str += "Height='" + Height + "',";
			if (X != null) {
				str += "X='" + X + "',";
				str += "Y='" + Y + "',";
			}
			str += "Content=" + GetContent();
			return str + "]";
		}

		public string XmlID { get; private set; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (HasXmlID() && XmlID == id)
				return this;
			return content?.GetXmlIdInChildren(id);
		}
	}
}
