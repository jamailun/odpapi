﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using ODP.API;

namespace ODP.Generics {
	public class Mask : IMask {

		// Basique pour le moment, je souhaite juste récupérer les images
		public string Name { get; }
		private IList<IFrame> frames;
		public IStyle PageStyle { get; }

		internal Mask(XElement element, IStylesLibrairie context) {
			frames = new List<IFrame>();
			Name = XmlParser.GetAttributeValue(element, "name");
			string styleName = XmlParser.GetAttributeValue(element, "style-name");
			PageStyle = context.GetWithName(styleName);
			if(PageStyle.HasFillImage()) {
				IFrame frame = new TextureFrame(PageStyle.GetFillImage());
				DebugProgram.Debug($"<Mask {Name} : +1 background ({PageStyle.GetFillImage().Name})>");
				frames.Add(frame);
			}

			foreach(XElement child in XmlParser.GetChildren(element)) {
				if(child.Name.LocalName == "frame") {
					Frame frame = new Frame(context, child, PageStyle);
					if (frame.GetContent().GetType() != typeof(ImageBox))
						continue;
					DebugProgram.Debug($"<Mask {Name} : +1 frame>");
					frames.Add(frame);
				}
			}
		}

		public IEnumerable<IFrame> GetFrames() {
			return frames.AsEnumerable();
		}

	}
}
