﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ODP.API;

namespace ODP.Generics {
	public class TextBox : ISlideElement, APIStyleable, APIXmlUnique {

		private readonly IStylesLibrairie context;
		private readonly IList<IList<IPLine>> paragraphs;
		private bool _visibleByDefault = true;
		public bool VisibleByDefault {
			get {
				if (! _visibleByDefault)
					return false;
				foreach(IList<IPLine> ll in paragraphs) { // si jamais une des lignes n'est pas affichée par défault, alors ce sera le cas pour TOUTE la boite de texte.
					foreach (IPLine l in ll) {
						if ( ! l.VisibleByDefault)
							return false;
					}
				}
				return true;
			}
			set { _visibleByDefault = value; }
		}

		public TextBox(IStylesLibrairie context, XElement source, IStyle parentStyle) {
			this.context = context;
			paragraphs = new List<IList<IPLine>>();
			XmlID = XmlParser.GetAttributeValueNullable(source, "id");
			string ownStyle = XmlParser.GetAttributeValueNullable(source, "style-name");
			Style = ownStyle != null ? parentStyle.DerivateWithChild(context.GetWithName(ownStyle)) : parentStyle;

			foreach (XElement element in XmlParser.GetChildren(source)) {
				AddElement(element);
			}
		}

		private void AddElement(XElement element) {
			string name = element.Name.LocalName;
			if(name == "p") {
				AddLine(element);
				return;
			}

			if (name != "list") {
				throw new System.Exception("new TextBox > AddElement > Unexpected tag name : " + name + ".");
			}

			IStyle listStyleParent = context.GetWithName( XmlParser.GetAttributeValue(element, "style-name") );
			IStyleList listStyle = listStyleParent.GetStyleList();

			foreach(XElement listItemElement in XmlParser.GetChildren(element)) {
				if (listItemElement.Name.LocalName == "list-header")
					// TODO ajouter les 'list-header' ?
					continue;
				if (listItemElement.Name.LocalName != "list-item")
					throw new System.Exception("new TextBox > AddElement > foreach > Unexpected tag name : " + listItemElement.Name.LocalName + ".");
				paragraphs.Add(new List<IPLine>());
				int listPosition = 1; // Je ne sais pas si la listPosition est ici ou avant le foreach précédent...
				foreach (XElement pElement in XmlParser.GetChildren(listItemElement)) {
					AddLine(pElement, listStyle.GetItem(listPosition++));
				}
			}
		}

		private void AddLine(XElement pElement) {
			AddLine(pElement, null);
		}

		private void AddLine(XElement pElement, IStyleListItem item) {
			if(paragraphs.Count == 0)
				paragraphs.Add(new List<IPLine>());
			paragraphs.Last().Add( new PLine(context, pElement, Style, item) );
		}

		public IStyle Style { get; }

		public IList<IList<IPLine>> GetParagraphs() {
			return paragraphs;
		}

		public override string ToString() {
			string str = "TextBox[Style="+Style.Name+",paragraphs:{";
			bool firstt = true;
			foreach (IList<IPLine> lines in GetParagraphs()) {
				if (firstt) firstt = false; else str += ", ";
				str += "(";
				bool first = true;
				foreach (IPLine line in lines) {
					if (first) first = false; else str += ", ";
					str += "'" + line + "'";
				}
				str += ")";
			}
			return str + "}]";
		}

		public string XmlID { get; private set; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (XmlID != null && XmlID == id)
				return this;
			foreach (IList<IPLine> paragraph in paragraphs) {
				foreach (IPLine line in paragraph) {
					APIXmlUnique son = line.GetXmlIdInChildren(id);
					if (son != null)
						return son;
				}
			}
			return null;
		}

	}
}
