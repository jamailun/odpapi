﻿using System.Collections.Generic;
using System.Xml.Linq;
using ODP.API;
using ODP.Transitions;

namespace ODP.Generics {
	public class Slide : ISlide {
		
		private readonly IStylesLibrairie context;
		private readonly IList<IFrame> elements;
		public ITransitionsTimingRoot TimingRoot { get; private set; }
		public IMask Mask { get; private set; }
		public bool VisibleByDefault { get; set; }

		public Slide(IStylesLibrairie context, XElement element, int position) {
			this.context = context;
			elements = new List<IFrame>();
			Name = XmlParser.GetAttributeValue(element, "name");
			Style = context.GetWithName(XmlParser.GetAttributeValue(element, "style-name"));
			MasterPageName = XmlParser.GetAttributeValue(element, "master-page-name");
			LayoutName = XmlParser.GetAttributeValueNullable(element, "presentation-page-layout-name");
			XmlID = XmlParser.GetAttributeValueNullable(element, "id");
			Position = position;

			Mask = context.GetMaskWithName(MasterPageName);
			if (Mask != null) {
				Style = Mask.PageStyle.DerivateWithChild(Style);
			}

			//	DebugProgram.Debug("new slide : " + Name + ", nom du style associé : " + Style.Name);
			//	DebugProgram.Debug($"SLIDESTYLE={Style.ToString()}");

			DebugProgram.Debug("+ Slide (" + Name + ")" + (Mask != null ? $"<-- ({Mask.Name})" : ""));
			foreach (XElement child in XmlParser.GetChildren(element)) {
				if (child.Name.LocalName != "notes") // la partie de 'notes' est inutile dans notre cas.
					AddFrame(child);
			}
			VisibleByDefault = true;

			// Cette liste va conserver toutes les ressources qui seront cachées au début.
			// Pour que quelque chose soit cahcé au début, il faut que la PREMIERE modification de visibilité
			// dans la listela fasse APPARAITRE. dans ce cas et quoi qu'il arrive ensuite, ça veut dire que l'objet était caché au début.
			IList<string> hided = new List<string>();

			// Détection des éléments à cacher par défaut.
			if (TimingRoot != null) {
				foreach(SequencesBlock sq in TimingRoot.RawSequence.GetSequencesBlocks()) {
					foreach(NodesSequence ns in sq.GetNodesSequences()) {
						foreach (ITransitionNode tn in ns.GetNodes()) {
							APIXmlUnique target = GetXmlIdInChildren(tn.TargetElementID);
							DebugProgram.PrintString($"Slide {Name} ===> id='{tn.TargetElementID}'");
							if(target == null)
								throw new System.Exception("new Slide > Cannot find the target of ID ["+tn.TargetElementID+"] during initialization of slide '"+Name+"'.");
							if(tn.GetType() == typeof(NodeSet)) {
								NodeSet set = (NodeSet)tn;
								if (set.AttributeName == "visibility" && set.AttributeValue == "visible") {
									target.VisibleByDefault = false;
								}
							}
						}
					}
				}
			}
		}

		private void AddFrame(XElement element) {
			string name = element.Name.LocalName;
			switch (name) {
				case "forms": // Pas utilisé pour le moment
					return;
				case "par":
					string nodeType = XmlParser.GetAttributeValue(element, "node-type");
					if (nodeType != "timing-root")
						throw new System.Exception($"new Slide > AddFrame > 'par' > Unexpected attribute value : {nodeType}.");

					TimingRoot = new TimingRoot(this, element);
					DebugProgram.Debug(" + transitions's root : " + TimingRoot);
					return;
				case "frame":
					elements.Add(new Frame(context, element, Style));
					DebugProgram.Debug(" + frame");
					return;
				case "custom-shape":
				case "line":
					elements.Add(new CustomShapeFrame(context, element, Style));
					DebugProgram.Debug(" + custom shape frame");
					return;
				default:
					throw new System.Exception("new Slide > AddFrame > Unexpected tag name : " + name + ".");
			}
		}

		public int Position { get; }

		public string LayoutName { get; }

		public string Name { get; }

		public string MasterPageName { get; }

		public IStyle Style { get; }

		public IList<IFrame> GetElements() {
			return elements;
		}

		public override string ToString() {
			string str = "Slide[\n\t";
			str += "name = '" + Name + "',\n\t";
			str += "layer = '" + LayoutName + "',\n\t";
			str += "style = '" + Style.Name + "',\n\t";
			str += "master-page-name = " + MasterPageName + ",\n\t";
			str += "position = " + Position + ",\n\t";
			str += "elements : {\n\t\t";
			int n = 0;
			foreach(IFrame frame in elements) {
				str += frame;
				if (n++ < elements.Count - 1)
					str += ",\n\t\t";
				else
					str += "\n\t";
			}
			str += "}\n";
			return str + "]";
		}

		public string XmlID { get; private set; }

		public bool HasXmlID() {
			return XmlID != null;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) {
			if (XmlID != null && XmlID == id)
				return this;
			foreach (IFrame frame in elements) {
				APIXmlUnique son = frame.GetXmlIdInChildren(id);
				if (son != null)
					return son;
			}
			return null;
		}

	}
}
