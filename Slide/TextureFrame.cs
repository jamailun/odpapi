﻿using ODP.API;
using ODP.Utils;

namespace ODP.Generics {
	public class TextureFrame : IFrame {

		private readonly ITexture content;
		public bool VisibleByDefault { get; set; }

		internal TextureFrame(ITexture texture) {
			this.content = texture;
			// Défault :
			Style = Generics.Style.Empty();
			X = new Size(0, StyleUnit.Centimeter);
			Y = new Size(0, StyleUnit.Centimeter);
			//Height = new Size(100, StyleUnit.Percentage);
			//Width = new Size(100, StyleUnit.Percentage);
			Height = new Size(100, StyleUnit.Percentage);
			Width = new Size(100, StyleUnit.Percentage);
			Rotation = new Size(0, StyleUnit.Radians);
			VisibleByDefault = true;
		}

		public string TextStyleName { get; }
		public string PresentationClass { get; }
		public string LayoutName { get; }
		public Size Rotation { get; }
		public Size Width { get; }
		public Size Height { get; }
		public Size X { get; }
		public Size Y { get; }
		public IStyle Style { get; }
		public string XmlID { get; }

		public ISlideElement GetContent() {
			return content;
		}

		public APIXmlUnique GetXmlIdInChildren(string id) { return null; }
		public bool HasXmlID() { return false; }

		public override string ToString() {
			string str = "TextureFrame[";
			str += "Style='" + Style.Name + "',";
			str += "Width='" + Width + "',";
			str += "Height='" + Height + "',";
			str += "Content=" + GetContent();
			return str + "]";
		}
	}
}
