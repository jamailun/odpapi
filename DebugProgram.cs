using ODP.API;
using ODP.Generics;
using System.Linq;

namespace ODP {

	/// Note : cette classe est vouée à un usage interne uniquement, afin de permettre de rapides debugs.

	internal class DebugProgram {

		static void Main(string[] args) {

			// Chemin du fichier à 
			//string inputFile = "F:/tests/test_visible.odp";
			//string inputFile = "F:/tests/realOne.odp";
			string inputFile = "C:/Users/timot/Documents/Unity/Slidermax on C/Assets/slidermax/Docs/xd.odp";

			ODPDocument parsed = ODPParser.ParseODPFile(inputFile, true);
			ISlide slide = parsed.GetSlideNumber(1);
			//PrintString(parsed.GetSlideNumber(1).TimingRoot.ToString());
			//PrintString(parsed.GetSlideNumber(2).TimingRoot.ToString());
			foreach(var el in slide.GetElements()) {
				PrintString(el.ToString() + " > " + el.VisibleByDefault);
			}
			//parsed.Print();

			//inputFile = "C:/Users/timot/Documents/Unity/Slidermax on C/Assets/slidermax/Docs/xd.odp";
			//parsed = ODPParser.ParseODPFile(inputFile, true);
			//PrintString(parsed.GetSlideNumber(15).TimingRoot.ToString());
			//PrintString(parsed.GetSlideNumber(15).GetXmlIdInChildren("id1").ToString() + " > " + parsed.GetSlideNumber(15).GetXmlIdInChildren("id1").VisibleByDefault);


			PrintString("Fin.");
		}

		internal static void PrintString(string str) {
#if DEBUG
			System.Diagnostics.Debug.WriteLine(str);
#else
			System.Console.WriteLine(str);
#endif
		}

		internal static void Debug(string str) {
			//System.Diagnostics.Debug.WriteLine("[DEBUG] " + str);
		}
	}

}
